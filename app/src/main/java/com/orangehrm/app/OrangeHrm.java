package com.orangehrm.app;

import com.orangehrm.page.LoginPage;
import com.qartrock.tools.pageobject.AbstractApp;

import ru.yandex.qatools.allure.annotations.Step;

public class OrangeHrm extends AbstractApp {

	private static final String URL = "http://opensource.demo.orangehrmlive.com/";

	@Step("Open Login Page")
	public LoginPage openLoginPage() {
		navigateTo(URL);
		return new LoginPage();
	}

	@Step("Open page by URL: {0}")
	private void navigateTo(String url) {
		getDriver().get(url);
	}

}
