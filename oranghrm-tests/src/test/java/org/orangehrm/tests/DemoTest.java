package org.orangehrm.tests;

import static com.qartrock.assertions.Assertions.shouldBeEqual;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.orangehrm.app.OrangeHrm;
import com.orangehrm.page.LoginPage;

public class DemoTest {

	protected OrangeHrm orangeHrm = new OrangeHrm();

	@Test
	public void verifyThatUserCanOpenLoginPage() {
		LoginPage loginPage = orangeHrm.openLoginPage();

		String expectedTitle = "OrangeHRM";
		String actualTitle = loginPage.getBrowserTitle();

		shouldBeEqual(actualTitle, expectedTitle, "Browser tab title should be as expected.");
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		OrangeHrm.close();
	}

}
