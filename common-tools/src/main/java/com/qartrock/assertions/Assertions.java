package com.qartrock.assertions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.not;
import static org.testng.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.qartrock.tools.genericmodel.GenericModel;

import ru.yandex.qatools.allure.annotations.Step;

/**
 * Custom assertions class. Contains custom assertions that are useful in
 * end-to-end tests.
 * 
 * @author rromanik
 *
 */
public class Assertions {

	/**
	 * Asserts that {@code actual} is equal to {@code expected}. {@code message}
	 * will appear in the report as a step, regardless of the status (PASSED,
	 * FAILED, SKIPPED) of test. Thus, the best format for the message is to use
	 * SHOULD verb, as in 'something should be something else'.
	 * 
	 * @param actual
	 * @param expected
	 * @param message
	 *            in format 'something should be something else'
	 */
	@Step("{2}")
	public static void shouldBeEqual(Object actual, Object expected, String message) {
		Assert.assertEquals(actual, expected, "\n" + message + "\n");
	}

	/**
	 * Asserts that {@code condition} is {@code true}. {@code message} will appear
	 * in the report as a step, regardless of the status (PASSED, FAILED, SKIPPED)
	 * of test. Thus, the best format for the message is to use SHOULD verb, as in
	 * 'something should be true'.
	 * 
	 * @param condition
	 * @param message
	 *            in format 'something should be true'
	 */
	@Step("{1}")
	public static void shouldBeTrue(boolean condition, String message) {
		Assert.assertTrue(condition, "\n" + message + "\n");
	}

	/**
	 * Asserts that {@code condition} is {@code false}. {@code message} will appear
	 * in the report as a step, regardless of the status (PASSED, FAILED, SKIPPED)
	 * of test. Thus, the best format for the message is to use SHOULD verb, as in
	 * 'something should be false'.
	 * 
	 * @param condition
	 * @param message
	 *            in format 'something should be false'
	 */
	@Step("{1}")
	public static void shouldBeFalse(boolean conditiion, String message) {
		Assert.assertFalse(conditiion, "\n" + message + "\n");
	}

	/**
	 * Asserts that two {@link GenericModel} are equal. First, the keys are
	 * analyzed. If two models have different keysets, the method throws Assertation
	 * error. If the models have the same keysets, every key value is compared of
	 * two models.
	 * 
	 * @param actual
	 * @param expected
	 */
	@Step("Actual Data {0} should be equal to expected data {1}")
	public static <T extends Enum<T>> void modelsShouldBeEqual(GenericModel<T> actual, GenericModel<T> expected) {
		modelsShouldBeEqual(actual, expected, "");
	}

	/**
	 * Asserts that two {@link GenericModel} are equal. First, the keys are
	 * analyzed. If two models have different keysets, the method throws Assertation
	 * error. If the models have the same keysets, every key value is compared of
	 * two models.
	 * 
	 * @param actual
	 * @param expected
	 * @param message
	 */
	@Step("{2}")
	public static <T extends Enum<T>> void modelsShouldBeEqual(GenericModel<T> actual, GenericModel<T> expected,
			String message) {
		setsShouldBeEqual(actual.includedFields(), expected.includedFields());

		SoftAssert soft = new SoftAssert();
		for (T field : actual.includedFields()) {
			soft.assertEquals(actual.get(field), expected.get(field),
					"\nNot equal values for field <" + field + ">:\n");
		}

		soft.assertAll();
	}

	private static void setsShouldBeEqual(Set<?> actualKeys, Set<?> expectedKeys) {
		SoftAssert soft = new SoftAssert();
		// verify that each key is expected
		for (Object actKey : actualKeys) {
			soft.assertTrue(expectedKeys.contains(actKey),
					"\nField <" + actKey + "> is not expected, but is present in the actual result.\n");
		}

		for (Object expKey : expectedKeys) {
			soft.assertTrue(actualKeys.contains(expKey),
					"\nField <" + expKey + "> is expected, but is not present in the actual result.\n");
		}
		soft.assertAll();
	}

	/**
	 * Verifies that {@code actualList} equals to {@code expectedList}. Equality
	 * means that each item from {@code actualList} is expected (i.e. is present in
	 * {@code expectedList}), and each item from {@code expectedList} is present in
	 * {@code actualList}. The order may be ignored.
	 * 
	 * @param actualList
	 * @param expectedList
	 * @param message
	 */
	@Step("{2}\n[Actual list {0} should be equal to expected list {1}]")
	public static <T> void listsShouldBeEqual(List<T> actualList, List<T> expectedList, String message) {
		listsShouldBeEqualIgnoringOrder(actualList, expectedList);
	}

	private static <T> void listsShouldBeEqualIgnoringOrder(List<T> actualList, List<T> expectedList) {
		SoftAssert soft = new SoftAssert();

		for (Object actualElement : actualList) {
			soft.assertTrue(expectedList.contains(actualElement),
					"\nElement <" + actualElement + "> is not expected, but is present in actual result. \n");
		}

		for (Object expectedElement : expectedList) {
			soft.assertTrue(actualList.contains(expectedElement),
					"\nElement <" + expectedElement + "> is expected, but is not present in actual result. \n");
		}
		soft.assertAll();
	}

	/**
	 * Asserts that two {@link GenericModel} are equal. The comparison is performed
	 * only by {@code fieldsToCompare} subset of keys. Thus, two models may not be
	 * equal in general, but if the values corresponding to keys from
	 * {@code fieldsToCompare} are equal between two models, the test will pass.
	 * 
	 * @param actual
	 * @param expected
	 * @param fieldsToCompare
	 */
	@Step("Actual data {0} should be equal to expected data {1}. Fields to compare: {3}")
	public static <T extends Enum<T>> void modelsShouldBeEqual(GenericModel<T> actual, GenericModel<T> expected,
			Set<T> fieldsToCompare) {
		SoftAssert soft = new SoftAssert();
		for (T field : fieldsToCompare) {
			soft.assertEquals(actual.get(field), expected.get(field),
					"\nNot equal values for field <" + field + ">:\n");
		}

		soft.assertAll();
	}

	@SuppressWarnings("unchecked")
	@Step("{2}\n[List {0} should contain exactly one element {1}]")
	public static <T> void listShouldContainExactOneElement(List<T> list, T item, String message) {
		assertThat("\n" + message + "\n", list, allOf(not(emptyIterable()), contains(item)));
	}

	@Step("{2}\n[List {0} should contain item {1}]")
	public static <T> void listShouldContain(List<T> list, T item, String message) {
		assertTrue(list.contains(item), message);
	}

	@Step("List {0} should contain all elements {1}")
	public static <T> void listShouldContainAll(List<T> list, List<T> sublist) {
		for (T item : sublist)
			listShouldContain(list, item, "List should contain item <" + item + ">");
	}

	/**
	 * Verifies if each element in {@link List} contains {@code text}. If the list
	 * is empty, {@link AssertionError} is thrown.
	 * 
	 * @param list
	 * @param text
	 * @param message
	 */
	@Step("{2}")
	public static void eachElementShouldContain(List<String> list, String text, String message) {
		SoftAssert soft = new SoftAssert();

		shouldBeTrue(!list.isEmpty(), message + "\nList must not be empty.");

		for (String item : list) {
			soft.assertTrue(item.contains(text), "Item '" + item + "' should contain text '" + text + "'");
		}
		soft.assertAll();
	}

	/**
	 * Verifies if each element in {@link List} contains {@code text}, with the
	 * option to ignore case. If the list is empty, {@link AssertionError} is
	 * thrown.
	 * 
	 * @param list
	 * @param text
	 * @param message
	 * @param ignoreCase
	 */
	@Step("{2}")
	public static void eachElementShouldContain(List<String> list, String text, String message, boolean ignoreCase) {
		SoftAssert soft = new SoftAssert();

		shouldBeTrue(!list.isEmpty(), message + "\nList must not be empty.");
		if (ignoreCase) {
			for (String item : list) {
				soft.assertTrue(StringUtils.containsIgnoreCase(item, text),
						"Item '" + item + "' should contain text '" + text + "'");
			}
		} else {
			for (String item : list) {
				soft.assertTrue(item.contains(text), "Item '" + item + "' should contain text '" + text + "'");
			}
		}
		soft.assertAll();
	}

	/**
	 * Verifies if each element in {@link List} contains {@code text}, ignoring
	 * case. If the list is empty, the assertion is successful.
	 * 
	 * @param list
	 * @param text
	 * @param message
	 */
	@Step("{2}")
	public static void eachElementShouldContainIgnoringCaseOrEmpty(List<String> list, String text, String message) {
		if (list.isEmpty())
			return;
		eachElementShouldContain(list, text, message, true);
	}

}
