package com.qartrock.assertions;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.FluentWait;

import ru.yandex.qatools.allure.annotations.Step;

public class FileAssertions {

	@Step("File should exists by the specified path")
	public static void fileShouldExistShortly(File file) {
		String filePath = file.getAbsolutePath();
		try {
			waitUntil(() -> file.exists());
		} catch (TimeoutException e) {
			throw new AssertionError("File should exist by the following path: " + filePath);
		}
	}
	
	@Step("{1}")
	public static void fileShouldExistShortly(File file, String message) {
		fileShouldExistShortly(file);
	}

	private static final int FLUENT_WAIT_TIMEOUT = 10;

	private static FluentWait<Object> getFluentWait() {
		return new FluentWait<Object>(new Object()).withTimeout(FLUENT_WAIT_TIMEOUT, TimeUnit.SECONDS);
	}

	private static void waitUntil(Supplier<Boolean> condition) {
		Function<Object, Boolean> conditionToBeTrue = (Object d) -> condition.get().equals(Boolean.TRUE);
		getFluentWait().until(conditionToBeTrue);
	}
}
