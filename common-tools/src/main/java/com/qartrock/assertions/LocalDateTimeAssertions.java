package com.qartrock.assertions;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import ru.yandex.qatools.allure.annotations.Step;

public class LocalDateTimeAssertions {

	@Step("Date {0} should be equal to date {1} if reduced to a specified format.")
	public static void shouldBeEqual(LocalDateTime actual, LocalDateTime expected, DateTimeFormatter formatter) {
		Assertions.shouldBeEqual(actual.format(formatter), expected.format(formatter), "Two dates should be equal");
	}
}
