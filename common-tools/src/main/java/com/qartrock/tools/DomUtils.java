package com.qartrock.tools;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DomUtils {

	public static Document newDocument() {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			return dBuilder.newDocument();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("Couldn't create an instance of Document", e);
		}
	}

	/**
	 * 
	 * @param parent
	 * @param childTagName
	 * @return child element
	 */
	public static Element createChildAndAppend(Element parent, String childTagName) {
		Document ownerDoc = parent.getOwnerDocument();
		Element child = ownerDoc.createElement(childTagName);
		parent.appendChild(child);

		return child;
	}

	public static Element createChildAndAppend(Document document, String childTagName) {
		Element child = document.createElement(childTagName);
		document.appendChild(child);
		return child;
	}

}
