package com.qartrock.tools;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.qartrock.tools.browser.DriverType;

@Retention(RetentionPolicy.RUNTIME)
public @interface ExcludeBrowsers {
	
	public DriverType[] browser();
	
}
