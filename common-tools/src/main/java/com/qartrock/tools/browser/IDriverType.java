package com.qartrock.tools.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public interface IDriverType {
	WebDriver getWebDriverObject(DesiredCapabilities desiredCapabilities);

	DesiredCapabilities getDesiredCapabilities();
}
