package com.qartrock.tools.browser;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import com.qartrock.tools.PropertiesManager;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.EdgeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import io.github.bonigarcia.wdm.OperaDriverManager;

public enum DriverType implements IDriverType {

	FIREFOX {
		private final List<String> knownNames = Arrays.asList("firefox", "mozillafirefox", "mozilla_firefox");

		public DesiredCapabilities getDesiredCapabilities() {
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			return capabilities;
		}

		public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("browser.download.folderList", 2);
			try {
				profile.setPreference("browser.download.dir",
						new File(System.getProperty("user.dir") + "/../").getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			profile.setPreference("browser.download.useDownloadDir", true);
			profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream");

			FirefoxOptions options = new FirefoxOptions();
			options.merge(capabilities);
			options.setProfile(profile);
			options.setAcceptInsecureCerts(true);

			FirefoxDriverManager.getInstance().version(getProperty("gecko_driver_version")).setup();

			WebDriver localDriver = new FirefoxDriver(options);

			return localDriver;
		}

		@Override
		protected boolean isKnownAs(String name) {
			return knownNames.contains(name);
		}
	},

	CHROME {
		private final List<String> knownNames = Arrays.asList("chrome", "googlechrome", "google_chrome");

		public DesiredCapabilities getDesiredCapabilities() {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			return capabilities;
		}

		public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			HashMap<String, Object> prefs = new HashMap<String, Object>();
			try {
				prefs.put("download.default_directory",
						new File(System.getProperty("user.dir") + "/../").getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			prefs.put("profile.default_content_settings.popups", 0);
			prefs.put("download.prompt_for_download", false);
			prefs.put("download.directory_upgrade", true);
			prefs.put("profile.password_manager_enabled", false);
			prefs.put("credentials_enable_service", false);
			prefs.put("safebrowsing.enabled", true);

			ChromeOptions options = new ChromeOptions();
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.BROWSER, Level.WARNING);
			options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			options.setExperimentalOption("prefs", prefs);
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-infobars");
			options.addArguments("--no-default-browser-check");

			ChromeDriverManager.getInstance().version(getProperty("chrome_driver_version")).setup();

			WebDriver localDriver = new ChromeDriver(options);

			return localDriver;
		}

		@Override
		protected boolean isKnownAs(String name) {
			return knownNames.contains(name);
		}
	},

	IE {
		private final List<String> knownNames = Arrays.asList("ie", "internetexplorer", "internet_explorer");

		public DesiredCapabilities getDesiredCapabilities() {
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
			capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			capabilities.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, "accept");

			capabilities.setCapability("disable-popup-blocking", true);
			return capabilities;
		}

		public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			InternetExplorerOptions options = new InternetExplorerOptions(capabilities);
			InternetExplorerDriverManager.getInstance().version(getProperty("ie_driver_version")).setup();
			WebDriver localDriver = new InternetExplorerDriver(options);
			return localDriver;
		}

		@Override
		protected boolean isKnownAs(String name) {
			return knownNames.contains(name);
		}
	},

	SAFARI {
		private final List<String> knownNames = Arrays.asList("safari");

		public DesiredCapabilities getDesiredCapabilities() {
			DesiredCapabilities capabilities = DesiredCapabilities.safari();
			capabilities.setCapability("safari.cleanSession", true);
			return capabilities;
		}

		public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {

			WebDriver localDriver = new SafariDriver(SafariOptions.fromCapabilities(capabilities));
			return localDriver;
		}

		@Override
		protected boolean isKnownAs(String name) {
			return knownNames.contains(name);
		}
	},

	OPERA {
		private final List<String> knownNames = Arrays.asList("opera");

		public DesiredCapabilities getDesiredCapabilities() {
			DesiredCapabilities capabilities = DesiredCapabilities.operaBlink();
			return capabilities;
		}

		public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			OperaOptions options = new OperaOptions();
			options.merge(capabilities);
			OperaDriverManager.getInstance().version(OPERA_DRIVER_VERSION).forceCache().setup();
			return new OperaDriver(options);
		}

		@Override
		protected boolean isKnownAs(String name) {
			return knownNames.contains(name);
		}
	},

	EDGE {
		private final List<String> knownNames = Arrays.asList("edge");

		public DesiredCapabilities getDesiredCapabilities() {
			DesiredCapabilities capabilities = DesiredCapabilities.edge();
			return capabilities;
		}

		public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			EdgeOptions options = new EdgeOptions();
			options.merge(capabilities);
			EdgeDriverManager.getInstance().setup();
			return new EdgeDriver(options);
		}

		protected boolean isKnownAs(String name) {
			return knownNames.contains(name);
		}
	};

	private static final String OPERA_DRIVER_VERSION = "2.32";

	public static DriverType from(String browser) {
		if (browser != null) {
			String trimmed = browser.trim().toLowerCase();
			for (DriverType candidate : DriverType.values()) {
				if (candidate.isKnownAs(trimmed))
					return candidate;
			}
		}
		throw new IllegalArgumentException("\nUnsupported browser: " + browser + "\n");
	}

	protected abstract boolean isKnownAs(String name);

	private static Properties properties;

	private static String getProperty(String key) {
		if (properties == null)
			properties = PropertiesManager.getPropertiesFrom("tools.properties");
		return properties.getProperty(key);
	}
}
