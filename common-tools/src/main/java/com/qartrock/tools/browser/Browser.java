package com.qartrock.tools.browser;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.qartrock.tools.ConfigurationManager;
import com.qartrock.tools.Platform;

public class Browser {
	private static final boolean IS_REMOTE = ConfigurationManager.isRemote();

	private WebDriver driver;

	/**
	 * To store console logs from geckodriver
	 */
	private ByteArrayOutputStream byteArrayOutputStream;

	public WebDriver getDriver() {
		if (driver != null)
			return driver;
		return driver = startNewInstance();
	}

	public void quit() {
		try {
			if (this.driver != null)
				this.driver.quit();
		} finally {
			this.driver = null;
		}
	}

	public boolean hasLoggedErrors() {
		return this.byteArrayOutputStream != null && this.byteArrayOutputStream.toByteArray().length != 0;
	}

	public ByteArrayOutputStream getLoggedErrors() {
		return this.byteArrayOutputStream;
	}

	public boolean isOpened() {
		return this.driver != null;
	}

	private WebDriver startNewInstance() {
		Platform platform = ConfigurationManager.getPlatformOrCurrentOsName();

		switch (platform) {
		case ANDROID:
			return Mobile.startAndroid();
		case IOS:
			return Mobile.startIos();
		case THIS_OS:
			return startDesktopWebBrowser();
		default:
			throw new RuntimeException("Unsupported platform: " + platform);
		}
	}

	private WebDriver startDesktopWebBrowser() {
		DriverType driverType = ConfigurationManager.getDriverType();
		if (driverType.equals(DriverType.FIREFOX)) {
			byteArrayOutputStream = new ByteArrayOutputStream();
			System.setErr(new PrintStream(byteArrayOutputStream));
		}

		DesiredCapabilities capabilities = driverType.getDesiredCapabilities();
		if (IS_REMOTE)
			return startRemoteDriver(capabilities);

		WebDriver driver = driverType.getWebDriverObject(capabilities);

		Dimension size = ConfigurationManager.getResolution();
		if (size == null) {
			driver.manage().window().maximize();
		} else {
			driver.manage().window().setSize(size);
		}

		return driver;
	}

	private static WebDriver startRemoteDriver(DesiredCapabilities desiredCapabilities) {
		WebDriver remoteDriver = null;
		try {
			remoteDriver = new RemoteWebDriver(new URL("http://10.10.0.71:4477/wd/hub"), desiredCapabilities);
		} catch (MalformedURLException e) {
			throw new RuntimeException("\nCouldn't start REMOTE WEB DRIVER!\n", e);
		}

		remoteDriver.manage().window().maximize();
		remoteDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return remoteDriver;
	}
}
