package com.qartrock.tools.browser;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class AdditionalConditions {

	public static ExpectedCondition<Boolean> documentReadyStateIsComplete() {
		return (WebDriver driver) -> "complete"
				.equals(JavascriptExecutor.class.cast(driver).executeScript("return document.readyState;"));
	}

	public static ExpectedCondition<Boolean> jQueryIsPresent() {
		return (WebDriver driver) -> Boolean.class
				.cast(JavascriptExecutor.class.cast(driver).executeScript("return (window.jQuery != null)"));
	}

	public static ExpectedCondition<Boolean> jQueryAjaxCallsHaveCompleted() {
		return (WebDriver driver) -> Boolean.class.cast(JavascriptExecutor.class.cast(driver)
				.executeScript("return (window.jQuery != null) && (jQuery.active === 0);"));
	}

	public static ExpectedCondition<Boolean> angularIsPresent() {
		return (WebDriver driver) -> Boolean.class
				.cast(((JavascriptExecutor) driver).executeScript("return (window.angular !== undefined)"));
	}

	public static ExpectedCondition<Boolean> angularHasFinishedProcessing() {
		return (WebDriver driver) -> Boolean.class.cast(JavascriptExecutor.class.cast(driver).executeScript(
				"return (window.angular !== undefined) && (angular.element(document).injector() !== undefined) && (angular.element(document).injector().get('$http').pendingRequests.length === 0)"));
	}
}
