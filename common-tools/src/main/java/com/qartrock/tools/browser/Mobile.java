package com.qartrock.tools.browser;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.qartrock.tools.ConfigurationManager;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class Mobile {

	private static final String DEFAULT_APPIUM_URL = "http://127.0.0.1:4723/wd/hub";

	public static WebDriver startChromeOnAndroid() {
		return startAndroid(androidWebCapabilities());
	}

	public static WebDriver startSafariOnIos() {
		return startIos(iosWebCapabilities());
	}

	public static WebDriver startAndroid() {
		if (StringUtils.isEmpty(ConfigurationManager.getBrowserName()))
			return startAndroid(androidNativeAppCapabilities());

		DriverType driver = ConfigurationManager.getDriverType();
		switch (driver) {
		case CHROME:
			return startChromeOnAndroid();
		default:
			throw new IllegalArgumentException(String.format("\nBrowser %s is not supported on Android.\n", driver));
		}

	}

	public static WebDriver startIos() {
		if (StringUtils.isEmpty(ConfigurationManager.getBrowserName()))
			return startIos(iosNativeAppCapabilities());

		DriverType driver = ConfigurationManager.getDriverType();
		switch (driver) {
		case SAFARI:
			return startSafariOnIos();
		default:
			throw new IllegalArgumentException(String.format("\nBrowser %s is not supported on iOS.\n", driver));
		}
	}

	private static WebDriver startAndroid(DesiredCapabilities capabilities) {
		AndroidDriver<MobileElement> driver = null;
		try {
			driver = new AndroidDriver<MobileElement>(new URL(DEFAULT_APPIUM_URL), capabilities);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		return driver;
	}

	private static WebDriver startIos(DesiredCapabilities capabilities) {
		IOSDriver<MobileElement> driver = null;
		try {
			driver = new IOSDriver<MobileElement>(new URL(DEFAULT_APPIUM_URL), capabilities);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		return driver;
	}

	private static DesiredCapabilities iosWebCapabilities() {
		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPad Air 2");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.SAFARI);
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.0");

		return capabilities;
	}

	private static DesiredCapabilities androidWebCapabilities() {
		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1.1");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus_5_API_25");
		capabilities.setCapability(AndroidMobileCapabilityType.AVD, "Nexus_5_API_25");
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);

		return capabilities;
	}

	private static DesiredCapabilities iosNativeAppCapabilities() {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.APP, getPathTo(ConfigurationManager.getTestAppName()));
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.0");
		caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPad Air 2");

		return caps;
	}

	private static DesiredCapabilities androidNativeAppCapabilities() {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.APP, getPathTo(ConfigurationManager.getTestAppName()));
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1.1");
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus_5_API_25");
		caps.setCapability(AndroidMobileCapabilityType.AVD, "Nexus_5_API_25");

		return caps;
	}

	private static String getPathTo(String nativeApp) {
		String userDir = System.getProperty("user.dir");
		String fileSeparator = System.getProperty("file.separator");
		String pathToApp = String.join(fileSeparator, userDir, "app", nativeApp);
		File app = new File(pathToApp);
		return app.getAbsolutePath();
	}
}
