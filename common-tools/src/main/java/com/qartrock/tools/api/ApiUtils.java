package com.qartrock.tools.api;

import static com.qartrock.tools.api.ApiLogUtils.log;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.StopWatch;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.BaseRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.body.MultipartBody;
import com.mashape.unirest.request.body.RequestBodyEntity;

public class ApiUtils {

	public static HttpResponse<JsonNode> getResponseAsJson(RequestBodyEntity request, String api) {
		log(request);
		return getResponseAndLogAllInfo(request, api, JsonNode.class);
	}

	public static HttpResponse<JsonNode> getResponseAsJson(HttpRequest request, String api) {
		log(request);
		return getResponseAndLogAllInfo(request, api, JsonNode.class);
	}

	public static HttpResponse<String> getResponseAsString(RequestBodyEntity request, String api) {
		log(request);
		return getResponseAndLogAllInfo(request, api, String.class);
	}

	public static HttpResponse<String> getResponseAsString(HttpRequest request, String api) {
		log(request);
		return getResponseAndLogAllInfo(request, api, String.class);
	}

	public static HttpResponse<String> getResponseAsString(MultipartBody request, Map<String, Object> fields,
			String api) {
		log(request, fields);
		return getResponseAndLogAllInfo(request, api, String.class);
	}

	public static HttpResponse<String> getResponseAsString(MultipartBody request, String api) {
		log(request);
		return getResponseAndLogAllInfo(request, api, String.class);
	}

	private static <T> HttpResponse<T> getResponseAndLogAllInfo(BaseRequest request, String api, Class<T> clazz) {
		HttpResponse<T> response = null;
		try {
			StopWatch watch = new StopWatch();
			watch.start();
			response = getResponse(request, clazz);
			watch.stop();
			String duration = String.valueOf(watch.getTime() / 1000.0);
			log(response);
			log(duration);
		} catch (UnirestException e) {
			throw new RuntimeException("\nProblem while requesting '" + api + "'\n", e);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	private static <T> HttpResponse<T> getResponse(BaseRequest request, Class<T> clazz) throws UnirestException {
		HttpResponse<?> response = null;

		if (clazz.isAssignableFrom(String.class)) {
			response = request.asString();
		} else if (clazz.isAssignableFrom(JsonNode.class)) {
			response = request.asJson();
		} else {
			throw new IllegalArgumentException();
		}
		return HttpResponse.class.cast(response);
	}

	public static String createCommaSeparatedString(List<String> list) {
		if (list.isEmpty())
			return "";

		StringBuilder builder = new StringBuilder();
		for (String string : list) {
			builder.append(string).append(",");
		}
		return builder.substring(0, builder.length() - 1);
	}
}
