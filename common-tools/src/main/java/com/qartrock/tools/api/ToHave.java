package com.qartrock.tools.api;

import java.util.Set;

public class ToHave {

	private final Set<String> keys;

	private ToHave(Set<String> keys) {
		this.keys = keys;
	}

	public static ToHave keys(Set<String> keys) {
		return new ToHave(keys);
	}

	Set<String> getKeys() {
		return this.keys;
	}
}
