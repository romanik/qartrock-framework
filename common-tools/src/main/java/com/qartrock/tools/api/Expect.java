package com.qartrock.tools.api;

import com.mashape.unirest.http.HttpResponse;

public class Expect {

	public static ResponseVerifier that(HttpResponse<String> httpResponse) {
		return new ResponseVerifier(httpResponse);
	}
}
