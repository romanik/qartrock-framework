package com.qartrock.tools.api;

import com.mashape.unirest.http.JsonNode;

public enum ToBe {
	JSON_ARRAY("JSON array") {
		@Override
		public boolean isMatchedBy(String text) {
			try {
				return new JsonNode(text).isArray();
			} catch (RuntimeException e) {
				return false;
			}
		}
	},
	JSON_OBJECT("JSON object") {
		@Override
		public boolean isMatchedBy(String text) {
			try {
				return !new JsonNode(text).isArray();
			} catch (RuntimeException e) {
				return false;
			}
		}
	},
	STRING("String") {
		@Override
		public boolean isMatchedBy(String text) {
			return text != null;
		}
	},
	NULL("null") {
		@Override
		public boolean isMatchedBy(String text) {
			return text == null;
		}
	};

	private final String toString;

	private ToBe(String toString) {
		this.toString = toString;
	}

	public abstract boolean isMatchedBy(String text);

	@Override
	public String toString() {
		return this.toString;
	}
}
