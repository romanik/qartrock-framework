package com.qartrock.tools.api;

public class HttpStatus {
	public static final String OK = "OK";
	public static final String CREATED = "Created";
	public static final String NO_CONTENT = "No Content";
	public static final String MULTIPLE_CHOICES = "Multiple Choices";
	public static final String FOUND = "Found";
	public static final String BAD_REQUEST = "Bad Request";
	public static final String UNAUTHORIZED = "Unauthorized";
	public static final String FORBIDDEN = "Forbidden";
	public static final String NOT_FOUND = "Not Found";
	public static final String REQUEST_TIMEOUT = "Request Timeout";
	public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
	public static final String NOT_IMPLEMENTED = "Not Implemented";
	public static final String SERVICE_UNAVAILABLE = "Service Unavailable";

	public static final int CODE_200 = 200;
	public static final int CODE_201 = 201;
	public static final int CODE_204 = 204;
	public static final int CODE_300 = 300;
	public static final int CODE_302 = 302;
	public static final int CODE_400 = 400;
	public static final int CODE_401 = 401;
	public static final int CODE_403 = 403;
	public static final int CODE_404 = 404;
	public static final int CODE_408 = 408;
	public static final int CODE_500 = 500;
	public static final int CODE_501 = 501;
	public static final int CODE_503 = 503;
}