package com.qartrock.tools.api;

import java.util.Objects;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;

public class CookieUtils {

	private CookieUtils() {
	}
	
	public static String getCookieFrom(CookieStore cookieStore, String cookieName) {
		return cookieName + "=" + getCookieValue(cookieStore, cookieName);
	}
	
	public static String getCookieValue(CookieStore cookieStore, String cookieName) {
		Objects.requireNonNull(cookieStore, "'cookieStore' must not be null");

		Cookie desiredCookie = cookieStore.getCookies().stream()
				.filter((Cookie cookie) -> cookieName.equals(cookie.getName())).findFirst()
				.orElseThrow(() -> new RuntimeException("No cookie with name <" + cookieName + ">"));

		return desiredCookie.getValue();
	}

}
