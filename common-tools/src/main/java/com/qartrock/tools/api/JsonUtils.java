package com.qartrock.tools.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;

public class JsonUtils {
	public static String getValue(HttpResponse<String> response, String key) {
		String result = "";
		try {
			result = new JSONObject(response.getBody()).getString(key);
		} catch (JSONException e) {
			throw new JSONException("JSON exception occurred. Message [" + e.getMessage() + "]", e);
		}
		return result;
	}
	
	public static JSONObject getJSONObject(HttpResponse<String> response) {
		JSONObject result = null;
		try {
			result = new JSONObject(response.getBody());
		} catch (JSONException e) {
			throw new JSONException("JSON exception occurred. Message [" + e.getMessage() + "]", e);
		}
		return result;
	}
	
	public static JSONArray getJSONArray(HttpResponse<String> response, String key) {
		JSONArray result = null;
		try {			
			JSONObject ob = new JSONObject(response.getBody());
			result = ob.getJSONArray(key);			
		} catch (JSONException e) {
			throw new JSONException("JSON exception occurred. Message [" + e.getMessage() + "]", e);
		}
		return result;
	}
}