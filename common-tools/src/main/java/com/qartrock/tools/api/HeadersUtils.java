package com.qartrock.tools.api;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import com.mashape.unirest.http.Headers;

public class HeadersUtils {

	private HeadersUtils() {
	}

	public static String getHeaderFrom(Headers headers, String header) {
		Objects.requireNonNull(headers, "'headers' must not be null");

		List<String> candidates = headers.get(header);
		if (candidates == null || candidates.isEmpty())
			throw new NoSuchElementException("No such header: <" + header + ">");
		return candidates.get(0);
	}

}
