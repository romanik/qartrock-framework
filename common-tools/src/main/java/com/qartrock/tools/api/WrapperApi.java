package com.qartrock.tools.api;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;

public class WrapperApi {

	public static HttpClient makeClient(){
		SSLContext sslcontext;
		CloseableHttpClient httpclient = null;
		try {
			sslcontext = SSLContexts.custom()
			        .loadTrustMaterial(null, new TrustSelfSignedStrategy())
			        .build();
				SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,new NoopHostnameVerifier());
			    httpclient = HttpClients.custom()
			            .setSSLSocketFactory(sslsf)
			            .build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			e.printStackTrace();
		}
	    return httpclient;
	}
	
}
