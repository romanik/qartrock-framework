package com.qartrock.tools.api;

import java.nio.charset.Charset;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.body.MultipartBody;
import com.mashape.unirest.request.body.RequestBodyEntity;

import ru.yandex.qatools.allure.annotations.Attachment;

public class ApiLogUtils {

	private static final String REQUEST = "Request";

	private static final String RESPONSE = "Response";

	private static final String DURATION = "Duration";

	private static final String URL = "Url";

	private static final String METHOD = "Method";

	private static final String HEADERS = "Headers";

	private static final String BODY = "Body";

	private static final String FIELDS = "Fields";

	private static final String STATUS_CODE = "Status Code";

	private static final String STATUS_MESSAGE = "Status Message";

	private ApiLogUtils() {
	}

	@Attachment(REQUEST)
	public static String log(RequestBodyEntity request) {
		return logHttpRequest(request.getHttpRequest()).append(logBody(request)).toString();
	}

	@Attachment(REQUEST)
	public static String log(MultipartBody request, Map<String, Object> fields) {
		logEntity(request.getEntity());
		return logHttpRequest(request.getHttpRequest()).append(logFields(fields)).toString();
	}

	@Attachment(REQUEST)
	public static String log(MultipartBody request) {
		return logHttpRequest(request.getHttpRequest()).append(logEntity(request.getEntity())).toString();
	}

	@Attachment(REQUEST)
	public static String log(HttpRequest request) {
		return logHttpRequest(request).toString();
	}

	private static StringBuilder logHttpRequest(HttpRequest request) {
		StringBuilder attachement = new StringBuilder();
		attachement.append(logHttpMethod(request)).append(logUrl(request)).append(logHeaders(request));
		return attachement;
	}

	@Attachment(value = RESPONSE, type = "text/plain")
	public static String log(HttpResponse<?> response) {
		StringBuilder attachement = new StringBuilder();
		attachement.append(logStatusCode(response)).append(logStatusMessage(response)).append(logHeaders(response))
				.append(logBody(response));

		return attachement.toString();
	}

	@Attachment(DURATION)
	static String log(String duration) {
		return log("API call duration", duration + " seconds");
	}

	private static String logHttpMethod(HttpRequest request) {
		return log(METHOD, request.getHttpMethod());
	}

	private static String logUrl(HttpRequest request) {
		return log(URL, request.getUrl());
	}

	private static String logBody(RequestBodyEntity request) {
		return log(BODY, request.getBody());
	}

	private static String logBody(HttpResponse<?> response) {
		return log(BODY, response.getBody());
	}

	private static String logHeaders(HttpRequest request) {
		return log(HEADERS, request.getHeaders());
	}

	private static String logHeaders(HttpResponse<?> response) {
		return log(HEADERS, response.getHeaders());
	}

	private static String logStatusCode(HttpResponse<?> response) {
		return log(STATUS_CODE, response.getStatus());
	}

	private static String logStatusMessage(HttpResponse<?> response) {
		return log(STATUS_MESSAGE, response.getStatusText());
	}

	private static String logFields(Map<String, Object> fields) {
		return log(FIELDS, fields);
	}

	private static String logEntity(HttpEntity entity) {
		String content = "";
		try {
			content = IOUtils.toString(entity.getContent(), Charset.defaultCharset());
		} catch (Exception e) {
			// nothing to do
		}
		return log(FIELDS, content);
	}

	private static String log(String key, Object value) {
		return String.format("\n%s : %s\n", key, value);
	}

}
