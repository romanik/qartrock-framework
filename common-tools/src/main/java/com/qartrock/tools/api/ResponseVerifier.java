package com.qartrock.tools.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Set;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.mashape.unirest.http.HttpResponse;

import ru.yandex.qatools.allure.annotations.Step;

public class ResponseVerifier {

	private static final String STATUS_MESSAGE_SHOULD_NOT_BE_VERIFIED = new String();

	private static final int STATUS_CODE_SHOULD_BE_IGNORED = -1;

	private final HttpResponse<String> httpResponse;

	private StringBuilder message = new StringBuilder();

	private int expectedStatusCode = STATUS_CODE_SHOULD_BE_IGNORED;

	private String expectedStatusMessage = STATUS_MESSAGE_SHOULD_NOT_BE_VERIFIED;

	private ToBe expectedBodyFormat;

	private ToHave expectedBodyContent;

	private Schema schema;

	ResponseVerifier(HttpResponse<String> httpResponse) {
		this.httpResponse = httpResponse;
	}

	public ResponseVerifier hasStatusCode(int expectedStatusCode) {
		this.expectedStatusCode = expectedStatusCode;
		return this;
	}

	private void checkStatusCode() {
		if (this.expectedStatusCode == STATUS_CODE_SHOULD_BE_IGNORED)
			return;
		logVerification("Verify that Status Code is <" + expectedStatusCode + ">");
		int actualCode = httpResponse.getStatus();
		if (expectedStatusCode != actualCode)
			message.append("\nResponse Status Code is not equal to the expected one.\nExpected: " + expectedStatusCode
					+ "\nActual: " + actualCode);
	}

	public ResponseVerifier hasStatusMessage(String expectedStatusMessage) {
		this.expectedStatusMessage = expectedStatusMessage;
		return this;
	}

	private void checkStatusMessage() {
		if (this.expectedStatusMessage == STATUS_MESSAGE_SHOULD_NOT_BE_VERIFIED)
			return;

		logVerification("Verify that Status Message is <" + expectedStatusMessage + ">");
		String actualMessage = httpResponse.getStatusText();
		if (!expectedStatusMessage.equals(actualMessage))
			message.append("\nResponse Status Message is not equal to the expected one.\nExpected Status Message: "
					+ expectedStatusMessage + "\nActual Status Message: " + actualMessage);
	}

	public ResponseVerifier withBody(ToBe expectedFormat) {
		this.expectedBodyFormat = Objects.requireNonNull(expectedFormat,
				"\nExpected format must not be null, use ToBe.NULL enum value instead.\n");
		return this;
	}

	private void checkBodyFormat() {
		if (this.expectedBodyFormat == null)
			return;
		logVerification("Verify that body format is <" + expectedBodyFormat + ">");
		String body = httpResponse.getBody();
		if (!this.expectedBodyFormat.isMatchedBy(body))
			message.append("\nResponse Body is not in expected format.\nExpected format: " + expectedBodyFormat);
	}

	public ResponseVerifier matchesSchema(String pathToSchema) {
		try {
			InputStream inputStream = new FileInputStream(new File(pathToSchema));
			JSONObject schemaObject = new JSONObject(new JSONTokener(inputStream));
			schema = SchemaLoader.load(schemaObject);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("\nCouldn't load JSON schema file <" + pathToSchema + ">\n");
		}
		return this;
	}

	public ResponseVerifier matchesSchema(JSONObject scheme) {
		schema = SchemaLoader.load(scheme);
		return this;
	}

	private void checkScheme() {
		if (this.schema == null)
			return;
		logVerification("Verify that the response body matches the expected JSON schema");
		try {
			try {
				JSONObject responseBody = new JSONObject(httpResponse.getBody());
				schema.validate(responseBody);
			} catch (JSONException e) {
				JSONArray responseBody = new JSONArray(httpResponse.getBody());
				schema.validate(responseBody);
			}
		} catch (ValidationException e) {
			StringBuilder sb = new StringBuilder();
			sb.append(e.getErrorMessage()).append("\n");

			e.getAllMessages().forEach(m -> sb.append(m).append("\n"));

			message.append("\nResponse JSON is not as expected scheme.\n" + sb.toString());
		}
	}

	/**
	 * Makes sense to use with {@link ToBe#JSON_OBJECT} only. In the case of
	 * {@link ToBe#JSON_ARRAY}, array is expected to consist of
	 * {@link ToBe#JSON_OBJECT} objects, and each object is to be verified.
	 * 
	 * @param expectedFormat
	 * @param data
	 * @return
	 */
	public ResponseVerifier withBody(ToBe expectedFormat, ToHave data) {
		withBody(expectedFormat);
		this.expectedBodyContent = data;
		return this;
	}

	private void checkBodyContent() {
		if (this.expectedBodyContent == null)
			return;
		checkBodyKeys();
	}

	public void verify() {
		checkStatusCode();
		checkStatusMessage();
		if (message.length() != 0)
			throw new AssertionError(message);

		checkBodyFormat();
		if (message.length() != 0)
			throw new AssertionError(message);

		checkBodyContent();
		if (message.length() != 0)
			throw new AssertionError(message);

		checkScheme();
		if (message.length() != 0)
			throw new AssertionError(message);
	}

	private void checkBodyKeys() {
		switch (expectedBodyFormat) {
		case JSON_ARRAY:
			checkJsonArrayObjectsForKeys();
			break;
		case JSON_OBJECT:
			checkJsonObjectForKeys();
			break;
		case NULL:
			break;
		case STRING:
			break;
		default:
			break;
		}
	}

	private void checkJsonObjectForKeys() {
		JSONObject jsonObject = new JSONObject(httpResponse.getBody());
		Set<String> data = expectedBodyContent.getKeys();

		for (String key : data) {
			if (!jsonObject.has(key))
				message.append("\nResponse body JSON should contain the following key: " + key + "\n");
		}
	}

	private void checkJsonArrayObjectsForKeys() {
		Set<String> expectedKeys = expectedBodyContent.getKeys();
		JSONArray jsonArray = new JSONArray(httpResponse.getBody());
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);

			for (String key : expectedKeys) {
				if (!jsonObject.has(key))
					message.append("\nResponse body JSON ARRAY:\nJSON OBJECT #" + i
							+ "\nShould contain the following key: " + key + "\n");
			}
		}
	}

	@Step("{0}")
	private void logVerification(String step) {
	}

}
