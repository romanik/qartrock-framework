package com.qartrock.tools;

import java.util.List;

import org.testng.annotations.DataProvider;

public class DataProviderUtils {

	/**
	 * Transforms list into Object[][] double array, to be compliant with
	 * {@link DataProvider}
	 * 
	 * @param dataInList
	 * @return data in a double array
	 */
	public static Object[][] testDataFromList(List<?> dataInList) {
		Object[][] testData = new Object[dataInList.size()][1];
		for (int i = 0; i < dataInList.size(); i++) {
			testData[i][0] = dataInList.get(i);
		}
		return testData;
	}
}
