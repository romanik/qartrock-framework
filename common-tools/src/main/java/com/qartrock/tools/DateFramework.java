package com.qartrock.tools;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Random;

public class DateFramework {

	/**
	 * Create exact date.
	 * 
	 * @param day
	 * @param month
	 * @param year
	 * @return {@link LocalDate}
	 */
	public static LocalDate createDate(int day, int month, int year) {
		return LocalDate.of(year, month, day);
	}

	/**
	 * Creates random date in range between two dates.
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @return {@link LocalDate}
	 */
	public static LocalDate createRandomDateInRange(LocalDate dateFrom, LocalDate dateTo) {
		long days = ChronoUnit.DAYS.between(dateFrom, dateTo);
		return dateFrom.plusDays(new Random().nextInt((int) days + 1));
	}

	/**
	 * Formats {@link LocalDate} into exact pattern.
	 * 
	 * @param date
	 *            - {@link LocalDate}
	 * @param pattern
	 *            - for example "dd/MM/YYYY"
	 * @return {@link String}
	 */
	public static String formatDateToPattern(LocalDate date, String pattern) {
		return date.format(DateTimeFormatter.ofPattern(pattern));
	}
	
	/**
	 * Formats {@link LocalDateTime} into exact pattern.
	 * 
	 * @param date
	 *            - {@link LocalDateTime}
	 * @param pattern
	 *            - for example "dd/MM/YYYY"
	 * @return {@link String}
	 */
	public static String formatDateToPattern(LocalDateTime date, String pattern) {
		return date.format(DateTimeFormatter.ofPattern(pattern));
	}

	/**
	 * Gets current date.
	 * 
	 * @return {@link LocalDate}
	 */
	public static LocalDate getToday() {
		return LocalDate.now();
	}

}
