package com.qartrock.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Handles reading the properties from 'config.properties' file
 * 
 * @author rromanik
 *
 */
public class PropertiesManager {

	private static Properties properties;

	private PropertiesManager() {
	}

	public static String getProperty(String key) {
		return getProperties().getProperty(key);
	}

	public static String getProtocol() {
		return getProperties().getProperty("protocol");
	}

	public static String getHost() {
		return getProperties().getProperty("host");
	}

	public static String getPort() {
		return getProperties().getProperty("port");
	}
	
	public static Properties getPropertiesFrom(String fileName) {
		return readProperties(fileName);
	}
	
	public static Properties getPropertiesFrom(File file) {
		return readProperties(file);
	}

	private static Properties getProperties() {
		return properties != null ? properties : (properties = readProperties("config.properties"));
	}

	private static Properties readProperties(String fileName) {
		Properties localProperties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(getPropertiesFile(fileName));
			// load a properties file
			localProperties.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return localProperties;
	}
	
	private static File getPropertiesFile(String fileName) {
		return new File(System.getProperty("user.dir") + File.separator + ".." + File.separator + fileName);
	}
	
	private static Properties readProperties(File file) {
		Properties localProperties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(file);
			// load a properties file
			localProperties.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return localProperties;
	}

}
