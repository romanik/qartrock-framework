package com.qartrock.tools;

public enum Platform {

	IOS("ios"), ANDROID("android"), THIS_OS(System.getProperty("os.name"));

	private final String name;

	private Platform(String name) {
		this.name = name;
	}

	/**
	 * Returns a {@code Platform} based on the specified {@code name}. The name
	 * should match the identified used to declare a {@code Platform} constant,
	 * ignoring the case and leading and trailing spaces.
	 * 
	 * @param string
	 * @return {@link Platform}
	 * @throws IllegalArgumentException
	 *             If no platform can be deduced from {@code string}
	 */
	public static Platform from(String string) {
		if (string != null) {
			String trimmed = string.trim();
			for (Platform candidate : Platform.values()) {
				if (trimmed.equalsIgnoreCase(candidate.name))
					return candidate;
			}
		}
		throw new IllegalArgumentException("\nUnsupported platform: " + string + "\n");
	}

	@Override
	public String toString() {
		return this.name;
	}
}
