package com.qartrock.tools;

import com.qartrock.tools.pageobject.IApp;

/**
 * Each test class should implement this interface to be effectively utilized by
 * Report.
 * 
 * @author rromanik
 *
 */
public interface IAppTest {

	public IApp getTestedAppInstance();
}
