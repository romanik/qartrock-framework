package com.qartrock.tools;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortUtils {
	
	public static Boolean isSortedDescending(List<String> listToSort) {
		listToSort.removeAll(Arrays.asList("", " "));
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) ((ArrayList<String>) listToSort).clone();
		if (isDateList(list)) {
			list = sortDates(list);
			Collections.reverse(list);
		} else 
		if (isNumberList(list)) {
			list = sortNumbers(list);
			Collections.reverse(list);
		} else
		if (isMonth(list)) {
			list = sortMonths(list);
			Collections.reverse(list);
		} else {
			list = sortNames(list);
			Collections.reverse(list);
		}
		return list.equals(listToSort);
	}
	
	public static Boolean isSortedAscending(List<String> listToSort) {
		listToSort.removeAll(Arrays.asList("", " "));
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) ((ArrayList<String>) listToSort).clone();
		if (isDateList(list)) {
			list = sortDates(list);
		} else 
		if (isNumberList(list)) {
			list = sortNumbers(list);
		} else
		if (isMonth(list)) {
			list = sortMonths(list);
		} else {
			list = sortNames(list);
		}
		return list.equals(listToSort);
	}
	
	public static List<String> sortDates(List<String> listToSort) {
		List<String> list = listToSort;
		Collections.sort(list, new Comparator<String>() {
	        DateFormat f = new SimpleDateFormat("dd.MM.yyyy");
	        @Override
	        public int compare(String o1, String o2) {
	            try {
					return f.parse(o1).compareTo(f.parse(o2));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
	        }
	    });
		return list;
	}
	
	public static List<String> sortNumbers(List<String> listToSort) {
		List<String> list = listToSort;
		Collections.sort(list, new Comparator<String>() {
	        @Override
	        public int compare(String o1, String o2) {
	            Integer x1 = Integer.parseInt(o1);
				Integer x2 = Integer.parseInt(o2);
				return x1.compareTo(x2);
	        }
	    });
		return list;
	}
	
	private static List<String> sortNames(List<String> listToSort) {
		List<String> list = listToSort;
		Collections.sort(list, new Comparator<String>() {
	        @Override
	        public int compare(String o1, String o2) {
	        	 String s1 = (String) o1;
	             String s2 = (String) o2;
	             return s1.toLowerCase().compareTo(s2.toLowerCase());
	        }
	    });
		return list;
	}
	
	private static List<String> sortMonths(List<String> listToSort) {
		List<String> list = listToSort;
		Collections.sort(list, new Comparator<String>() {
			DateFormat f = new SimpleDateFormat("MMM");
	        @Override
	        public int compare(String o1, String o2) {
	            try {
					return f.parse(o1).compareTo(f.parse(o2));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
	        }
	    });
		return list;
	}
	
	private static Boolean isDateList(List<String> list) {
		return list.stream().allMatch(e -> e.matches("^\\d?\\d.\\d{2}.\\d{4}$"));
	}
	
	private static Boolean isNumberList(List<String> list) {
		return list.stream().allMatch(e -> e.matches("\\d+"));
	}
	
	private static Boolean isMonth(List<String> list) {
		List<String> months = Arrays.stream(new DateFormatSymbols().getMonths()).collect(Collectors.toList());
		return list.stream().allMatch(e -> months.contains(e));
	}
}
