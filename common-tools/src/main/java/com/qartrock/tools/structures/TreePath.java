package com.qartrock.tools.structures;

import java.util.Arrays;
import java.util.Objects;

public class TreePath {

	private final String[] nodes;

	private TreePath(String[] nodes) {
		this.nodes = Objects.requireNonNull(nodes, "TreePath nodes argument must not be null");
	}

	public static TreePath of(String... nodes) {
		return new TreePath(nodes);
	}

	public String[] getNodeArray() {
		return Arrays.copyOf(this.nodes, this.nodes.length);
	}

	public String[] getNodeArrayWithoutLast() {
		return Arrays.copyOfRange(this.nodes, 0, this.nodes.length - 1);
	}

	public String getLastNode() {
		return this.nodes[this.nodes.length - 1];
	}

	public int size() {
		return this.nodes.length;
	}

	@Override
	public String toString() {
		return "TreePath [nodes=" + Arrays.toString(nodes) + "]";
	}

}
