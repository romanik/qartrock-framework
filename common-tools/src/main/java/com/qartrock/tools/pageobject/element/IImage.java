package com.qartrock.tools.pageobject.element;

import com.qartrock.tools.pageobject.GuiElement;

public interface IImage extends GuiElement {

	/**
	 * The location (URL) of the external resource
	 * @return location (URL) of the external resource
	 */
	String getSource();

	/**
	 * Returns an alternate text for an image
	 * 
	 * @return alternate text for an image
	 */
	String getAlt();
}
