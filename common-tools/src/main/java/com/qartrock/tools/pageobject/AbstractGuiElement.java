package com.qartrock.tools.pageobject;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.qartrock.tools.WebElementUtils;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.Widget;
import ru.yandex.qatools.allure.annotations.Step;

public abstract class AbstractGuiElement extends Widget implements GuiElement {

	private String name;

	private WebElementUtils webElementUtils;

	private FluentWait<WebDriver> waiter;

	protected static final long DEFAULT_TIMEOUT_TO_CLICK = 10;
	protected static final long DEFAULT_TIMEOUT_TO_WAIT = 10;

	public AbstractGuiElement(WebElement element) {
		super(element);
		PageFactory.initElements(new AppiumFieldDecorator(element), this);
	}

	public AbstractGuiElement(WebElement wrappedElement, String name) {
		this(wrappedElement);
		this.name = name;
	}

	protected WebElementUtils webElementUtils() {
		return webElementUtils != null ? webElementUtils : (webElementUtils = WebElementUtils.getInstance());
	}

	protected FluentWait<WebDriver> getWebDriverWait() {
		if (waiter == null)
			waiter = new FluentWait<WebDriver>(getWrappedDriver());
		return waiter.withTimeout(DEFAULT_TIMEOUT_TO_WAIT, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class);
	}

	protected FluentWait<WebDriver> getWebDriverWait(long timeout) {
		if (waiter == null)
			waiter = new FluentWait<WebDriver>(getWrappedDriver());
		return waiter.withTimeout(timeout, TimeUnit.SECONDS).ignoring(StaleElementReferenceException.class);
	}

	@Override
	public boolean exists() {
		try {
			getWrappedElement().isDisplayed();
			return true;
		} catch (NoSuchElementException ignored) {
			return false;
		}
	}

	@Override
	public boolean isShown() {
		step(toString(), "Check if shown");
		return webElementUtils().isElementShown(getWrappedElement());
	}

	@Override
	public boolean isEnabled() {
		step(toString(), "Check if enabled");
		return webElementUtils().isElementEnabled(getWrappedElement());
	}

	@Override
	public void click() {
		if (!isShown()) {
			throw new IllegalStateException("\n" + this.toString() + " should be shown before click.\n");
		}
		if (!isEnabled()) {
			throw new IllegalStateException("\n" + this.toString() + " should be enabled before click.\n");
		}
		step(toString(), "Click");
		// webElementUtils().hover(getWrappedElement());
		webElementUtils().clickOn(getWrappedElement());
	}

	@Override
	public void doubleClick() {
		if (!isShown()) {
			throw new IllegalStateException("\n" + this.toString() + " should be shown before click.\n");
		}
		if (!isEnabled()) {
			throw new IllegalStateException("\n" + this.toString() + " should be enabled before click.\n");
		}
		step(toString(), "Double-Click");
		webElementUtils().hover(getWrappedElement());
		webElementUtils().doubleClickOn(getWrappedElement());
	}

	@Override
	public String getText() {
		if (!isShown()) {
			throw new IllegalStateException("\n" + this.toString() + " should be shown before getting text.\n");
		}
		step(toString(), "Read text");
		return getWrappedElement().getText();
	}

	@Override
	public String getAttribute(String attributeName) {
		return getWrappedElement().getAttribute(attributeName);
	}

	@Override
	public <X> X getScreenshotAs(OutputType<X> target) {
		return getWrappedElement().getScreenshotAs(target);
	}

	@Override
	public void scrollIntoView() {
		webElementUtils().scrollIntoView(getWrappedElement());
	}

	@Override
	public void hoverOver() {
		step(toString(), "Hover over");
		webElementUtils().hover(getWrappedElement());
	}

	/**
	 * Use a constructor {@link AbstractGuiElement} that takes two parameters:
	 * {@link WebElement} {@code wrappedElement} and {@link String} {@code name}
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException}
	 * 
	 * @param condition
	 * @throws TimeoutException
	 */
	protected void waitUntil(Supplier<Boolean> condition) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait().until(conditionToBeTrue);
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException}
	 * 
	 * @param condition
	 * @param message
	 * @throws TimeoutException
	 */
	protected void waitUntil(Supplier<Boolean> condition, String message) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait().withMessage(message).until(conditionToBeTrue);
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException} after specified {@code timeout}
	 * 
	 * @param condition
	 * @param timeout
	 */
	protected void waitUntil(Supplier<Boolean> condition, long timeout) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait(timeout).until(conditionToBeTrue);
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException} after specified {@code timeout}
	 * 
	 * @param condition
	 * @param timeout
	 * @param message
	 */
	protected void waitUntil(Supplier<Boolean> condition, long timeout, String message) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait(timeout).withMessage(message).until(conditionToBeTrue);
	}

	/**
	 * For compatibility with {@link ExpectedConditions}
	 * 
	 * @param isTrue
	 */
	protected <V> void waitUntil(Function<? super WebDriver, V> isTrue) {
		getWebDriverWait().until(isTrue);
	}

	/**
	 * For compatibility with {@link ExpectedConditions}
	 * 
	 * @param isTrue
	 */
	protected <V> void waitUntil(Function<? super WebDriver, V> isTrue, long timeout) {
		getWebDriverWait(timeout).until(isTrue);
	}

	/**
	 * Tries to wait for {@code condition} to return {@code true} but ignore
	 * {@link TimeoutException} and simply returns
	 * 
	 * @param condition
	 */
	protected void tryWaitUntil(Supplier<Boolean> condition) {
		try {
			waitUntil(condition);
		} catch (TimeoutException e) {
			// Just return control to the calling code
		}
	}

	/**
	 * Tries to wait for {@code condition} to return {@code true} but ignore
	 * {@link TimeoutException} and simply returns after {@code timeout}
	 * 
	 * @param condition
	 */
	protected void tryWaitUntil(Supplier<Boolean> condition, long timeout) {
		try {
			waitUntil(condition, timeout);
		} catch (TimeoutException e) {
			// Just return control to the calling code
		}
	}

	@Deprecated
	protected void waitForStaleness(List<WebElement> list) {
		list.stream().forEach(e -> tryWaitUntil(ExpectedConditions.stalenessOf(e), 2));
	}

	/**
	 * For compatibility with {@link ExpectedConditions}
	 * 
	 * @param isTrue
	 */
	protected <V> void tryWaitUntil(Function<WebDriver, V> isTrue) {
		try {
			waitUntil(isTrue);
		} catch (TimeoutException e) {
			// Just return control to the calling code
		}
	}

	protected <V> void tryWaitUntil(Function<WebDriver, V> isTrue, long timeout) {
		try {
			waitUntil(isTrue, timeout);
		} catch (TimeoutException e) {
			// Just return control to the calling code
		}
	}

	/**
	 * It is special wait for cases when only magic or Chuck Norris can save us from
	 * problems
	 * 
	 * @param aWhile
	 */
	protected void sleep(int aWhile) {
		try {
			Thread.sleep(aWhile);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return this.name != null ? this.name : this.getClass().getSimpleName();
	}

	@Step("{0}: {1}")
	protected void step(String name, String stepDescription) {
	}

	public Alert getAlert() {
		try {
			waitForAlert();
			return getWrappedDriver().switchTo().alert();
		} catch (TimeoutException | NoAlertPresentException e) {
			throw new RuntimeException("There is no alert", e);
		}
	}

	private void waitForAlert() {
		getWebDriverWait(20).until(ExpectedConditions.alertIsPresent());
	}

	@Step
	protected void acceptAllAlerts() {
		boolean shouldWait = true;
		while (shouldWait) {
			try {
				getWebDriverWait(3).until(ExpectedConditions.alertIsPresent());
				getAlert().accept();
			} catch (TimeoutException | NoAlertPresentException e) {
				shouldWait = false;
			}
		}
	}

}
