package com.qartrock.tools.pageobject.element;

import com.qartrock.tools.pageobject.GuiElement;

/**
 * Describes a notification alert with a message and OK button
 */
public interface OkAlert extends GuiElement {

	public ILabel message();

	public IButton okButton();

	void ok();

	String getMessage();
}