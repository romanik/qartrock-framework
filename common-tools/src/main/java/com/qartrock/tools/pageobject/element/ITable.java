package com.qartrock.tools.pageobject.element;

import java.util.List;

import com.qartrock.tools.genericmodel.Model;
import com.qartrock.tools.pageobject.GuiElement;

public interface ITable<RowModel extends Model, Row extends IRow<RowModel>> extends GuiElement {
	boolean isTitleShown();

	String getTitle();

	List<Row> getRows();

	Row getRow(int index);

	Row getRow(RowModel row);

	List<RowModel> getModelList();

	boolean contains(RowModel row);
	
	List<RowModel> getUniqueModelList();
}
