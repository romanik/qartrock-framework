package com.qartrock.tools.pageobject.element;

import java.util.List;

import com.qartrock.tools.pageobject.GuiElement;

public interface Tree<N extends INode<N>> extends GuiElement {

	public List<N> rootLevelNodes();
	
	public List<String> rootLevelNodesNames();
	
	public N getRootNode(String node);
	
	public N expandNodes(String[] nodes);
}
