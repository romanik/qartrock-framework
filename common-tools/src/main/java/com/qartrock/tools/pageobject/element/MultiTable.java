package com.qartrock.tools.pageobject.element;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qartrock.tools.genericmodel.GenericModel;
import com.qartrock.tools.genericmodel.Model;

import ru.yandex.qatools.allure.annotations.Step;

public abstract class MultiTable<RowModel extends Model, Row extends IRow<RowModel>> extends Table<RowModel, Row> implements IMultiTable<RowModel, Row> {

	@FindBy(xpath = "//*[@class='pagination']")
	private WebElement pagination;

	public MultiTable(WebElement element) {
		super(element);
	}

	public MultiTable(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	@Override
	public List<RowModel> getModelList() {
		if (pagination().isFirstButtonEnabled()) {
			pagination().firstButton().click();
		}

		List<RowModel> models = new ArrayList<>();

		do {
			models.addAll(getVisibleModelList());
			pagination().nextButton().click();
		} while (pagination().isNextButtonEnabled());
		
		return models;
	}

	@Override
	public List<RowModel> getVisibleModelList() {
		return getRows().stream().map(Row::asDataModel).collect(Collectors.toList());
	}
	
	@Override
	@Step("Checks if table contains data: {0}")
	public boolean contains(RowModel row) {
		if (getVisibleModelList().contains(row)){
			return getVisibleModelList().contains(row);
		} else {
			if (pagination().isFirstButtonEnabled()) {
				pagination().firstButton().click();
			}
			
			do {
				if (getVisibleModelList().contains(row)){
					return getVisibleModelList().contains(row);
				}
				pagination().nextButton().click();
			} while (pagination().isNextButtonEnabled());
			return false;
		}
	}
	
	@Override
	@Step("Checks if table contains data: {0} /n for fields: {1}")
	public <T extends Enum<T>> boolean contains(RowModel row, Set<T> fieldsToIterate) {
		if (listContains(getVisibleModelList(), row, fieldsToIterate)) {
			return true;
		} else {
			if (pagination().isFirstButtonEnabled()) {
				pagination().firstButton().click();
			}
			
			do {
				if (listContains(getVisibleModelList(), row, fieldsToIterate)){
					return true;
				}
				pagination().nextButton().click();
			} while (pagination().isNextButtonEnabled());
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	private <T extends Enum<T>> boolean softEquals(RowModel modelOne, RowModel modelTwo, Set<T> fieldsToIterate) {
		GenericModel<T> thisModel = (GenericModel<T>) modelOne;
		GenericModel<T> otherModel = (GenericModel<T>) modelTwo;
		if (thisModel.equals(otherModel))
			return true;
		
		Set<T> commonFields = fieldsToIterate;
		
		if (fieldsToIterate == null) {
			commonFields = thisModel.includedFields().stream().filter(e -> otherModel.includedFields().contains(e)).collect(Collectors.toSet());
		} else {
			if (!thisModel.includedFields().containsAll(commonFields)) {
				throw new IllegalArgumentException("Provided model:" + thisModel + " should contain fields:" + commonFields);
			}
			if (!otherModel.includedFields().containsAll(commonFields)) {
				throw new IllegalArgumentException("Provided model:" + otherModel + " should contain fields:" + commonFields);
			}
		}
		
		System.out.println("Common fields = " + commonFields);
		
		return commonFields.stream().allMatch(e -> thisModel.get(e).equals(otherModel.get(e)));
	}
	
	private <T extends Enum<T>> boolean listContains(List<RowModel> modelList, RowModel model, Set<T> fieldsToIterate) {
		if (fieldsToIterate == null) {
			for (RowModel row : modelList) {
				if (softEquals(model, row, null))
					return true;
			}
		} else {
			for (RowModel row : modelList) {
				if (softEquals(model, row, fieldsToIterate))
					return true;
			}
		}
		
		return false;
	}
	
	@Override
	@Step("Soft check if table contains data: {0}")
	public boolean softContains(RowModel row) {
		if (listContains(getVisibleModelList(), row, null)) {
			return true;
		} else {
			if (pagination().isFirstButtonEnabled()) {
				pagination().firstButton().click();
			}
			
			do {
				if (listContains(getVisibleModelList(), row, null)){
					return true;
				}
				pagination().nextButton().click();
			} while (pagination().isNextButtonEnabled());
			return false;
		}
	}

	@Override
	@Step("Gets row by data: {0}")
	public Row getRow(RowModel row) {
		if (!contains(row))
			throw new NoSuchElementException("\nThere is no such row " + row + " in table.\n");

		int index = getVisibleModelList().indexOf(row);

		return getRow(index);
	}

	@Override
	@Step("Checks if Pagination is shown")
	public boolean isPaginationShown() {
		return webElementUtils().isElementShown(pagination);
	}

	private IPagination iPagination;

	@Override
	public IPagination pagination() {
		return iPagination != null ? iPagination : (iPagination = new Pagination(pagination, "Users Table Pagination"));
	}
}
