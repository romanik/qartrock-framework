package com.qartrock.tools.pageobject;

import static io.appium.java_client.pagefactory.utils.WebDriverUnpackUtility.unpackWebDriverFromSearchContext;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import ru.yandex.qatools.allure.annotations.Step;

abstract class PageObjectBase<T extends PageObjectBase<T>> {
	protected SearchContext searchContext;

	private FluentWait<WebDriver> waiter;

	protected static final long DEFAULT_TIMEOUT_TO_CLICK = 10;
	protected static final long DEFAULT_TIMEOUT_TO_WAIT = 10;

	protected PageObjectBase(SearchContext searchContext) {
		this.searchContext = searchContext;
		PageFactory.initElements(new AppiumFieldDecorator(this.searchContext), this);
	}

	protected WebDriver getWrappedDriver() {
		return unpackWebDriverFromSearchContext(searchContext);
	}

	protected WebElement getWrappedElement() {
		return (WebElement) searchContext;
	}

	protected JavascriptExecutor getJavascriptExecutor() {
		return (JavascriptExecutor) getWrappedDriver();
	}

	protected SearchContext getWrappedSearchContext() {
		return this.searchContext;
	}

	protected FluentWait<WebDriver> getWebDriverWait() {
		if (waiter == null)
			waiter = new FluentWait<WebDriver>(getWrappedDriver());
		return waiter.withTimeout(DEFAULT_TIMEOUT_TO_WAIT, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class);
	}

	/**
	 * Returns true if and only if element is present in DOM and is displayed.
	 * 
	 * @param element
	 * @return
	 */
	protected boolean isElementShown(WebElement element) {
		try {
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		} catch (StaleElementReferenceException e) {
			return isElementShown(element);
		}
	}

	protected boolean isElementEnabled(WebElement element) {
		return element.isEnabled();
	}

	/**
	 * Find WebElement that is focused on page
	 * 
	 * @return WebElement
	 */
	protected WebElement getFocusedWebElement() {
		return getWrappedDriver().switchTo().activeElement();
	}

	/**
	 * Returns the value of web element.
	 * 
	 * @param element
	 * @return value
	 */
	protected String getValueOf(WebElement element) {
		return element.getAttribute("value");
	}

	/**
	 * Returns the placeholder value of web element
	 * 
	 * @param element
	 * @return placeholder
	 */
	protected String getPlaceholderOf(WebElement element) {
		return element.getAttribute("placeholder");
	}

	/**
	 * Returns the text of web element.
	 * 
	 * @param element
	 * @return text
	 */
	protected String getTextOf(WebElement element) {
		return element.getText();
	}

	/**
	 * Sends keys to web element.
	 * 
	 * @param element
	 * @param keysToSend
	 */
	protected void sendKeysTo(WebElement element, String keysToSend) {
		element.sendKeys(keysToSend);
	}

	/**
	 * Clears the value of web element. Uses Selenium tools
	 * 
	 * @param element
	 */
	protected void clear(WebElement element) {
		element.clear();
	}

	/**
	 * Clears the value of web element and sends keys to it.
	 * 
	 * @param element
	 * @param keysToSend
	 */
	protected void clearAndSendKeysTo(WebElement element, String keysToSend) {
		clear(element);
		sendKeysTo(element, keysToSend);
	}

	/**
	 * Clears the value of web element and sends keys to it.
	 * 
	 * @param element
	 * @param keysToSend
	 */
	protected void setInputValue(WebElement element, String keysToSend) {
		clearAndSendKeysTo(element, keysToSend);
		try {
			getWebDriverWait().until(ExpectedConditions.attributeToBe(element, "value", keysToSend));
		} catch (TimeoutException e) {
			System.out.println("Value is " + element.getAttribute("value"));
			throw new TimeoutException("Value " + keysToSend + " was not set to input field");
		}
	}

	/**
	 * Performs click on element.
	 * 
	 * @param element
	 */
	@Step("Clicks on {0}")
	protected void clickOn(WebElement element) {
		element.click();
	}

	/**
	 * Waits for element to be clickable and then clicks on it.
	 * 
	 * @param element
	 *            WebElement to click on
	 * @param timeInSeconds
	 *            maximum time in seconds to wait
	 */
	protected void waitAndClick(WebElement element, long timeInSeconds) {
		new WebDriverWait(this.getWrappedDriver(), timeInSeconds).ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.elementToBeClickable(element)).click();
	}

	/**
	 * Waits for element to be clickable and then clicks on it. Uses
	 * <code>DEFAULT_TIMEOUT_TO_CLICK</code> as maximum time to wait.
	 * 
	 * @param element
	 */
	protected void waitAndClick(WebElement element) {
		waitAndClick(element, DEFAULT_TIMEOUT_TO_CLICK);
	}

	/**
	 * Clicks on web element with the help of JavaScript
	 * 
	 * @param element
	 */
	protected void clickJS(WebElement element) {
		getJavascriptExecutor().executeScript("arguments[0].click();", element);
	}

	/**
	 * Moves a web element into view with the help of JavaScript
	 * 
	 * @param e
	 * @return webelement
	 */
	protected WebElement scrollIntoView(WebElement e) {
		getJavascriptExecutor().executeScript("arguments[0].scrollIntoView(true)", e);
		return e;
	}

	protected void inputValueByJavaScript(String value) {
		getJavascriptExecutor().executeScript("arguments[0].setAttribute('value', arguments[1])",
				getWrappedDriver().findElement(By.cssSelector("td:nth-of-type(1) input[name='name']")), value);

	}

	protected String getCssValueOfPseudoElement(WebElement element, String pseudoElement, String cssProperty) {
		String script = "return window.getComputedStyle(arguments[0],'" + pseudoElement + "').getPropertyValue('"
				+ cssProperty + "')";
		String value = (String) getJavascriptExecutor().executeScript(script, element);

		return value;
	}

	protected String getCssValueOfPseudoElement(String cssSelector, String pseudoElement, String cssProperty) {
		String script = "return window.getComputedStyle(document.querySelector('" + cssSelector + "'),'" + pseudoElement
				+ "').getPropertyValue('" + cssProperty + "')";
		String value = (String) getJavascriptExecutor().executeScript(script);

		return value;
	}

	/**
	 * Waits for element to be invisible.
	 * 
	 * @param element
	 *            waits for this element
	 * @param timeInSeconds
	 *            maximum time in seconds to wait
	 */
	protected void waitForInvisibilityOf(WebElement element, long timeInSeconds) {
		try {
			new WebDriverWait(this.getWrappedDriver(), timeInSeconds).ignoring(StaleElementReferenceException.class)
					.ignoring(NoSuchElementException.class)
					.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
		} catch (TimeoutException e) {
			// nothing to do
		} catch (NoSuchElementException e) {
			// nothing to do
		}
	}

	/**
	 * Waits for element to be invisible. Uses
	 * <code>DEFAULT_TIMEOUT_TO_WAIT</code> as maximum time to wait.
	 * 
	 * @param element
	 *            waits for this element
	 */
	protected void waitForInvisibilityOf(WebElement element) {
		this.waitForInvisibilityOf(element, DEFAULT_TIMEOUT_TO_WAIT);
	}

	protected WebElement waitForVisibilityOf(WebElement element) {
		return waitForVisibilityOf(element, DEFAULT_TIMEOUT_TO_WAIT);
	}

	protected WebElement waitForVisibilityOf(WebElement element, long timeoutInSeconds) {
		try {
			new WebDriverWait(this.getWrappedDriver(), timeoutInSeconds).ignoring(StaleElementReferenceException.class)
					.until(ExpectedConditions.visibilityOf(element));
		} catch (TimeoutException e) {
			// nothing to do here
		}

		return element;
	}

	/**
	 * Not for use in production code.
	 * 
	 * @param aWhile
	 */
	@Deprecated
	protected void holdOnFor(int aWhile) {
		try {
			Thread.sleep(aWhile);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
