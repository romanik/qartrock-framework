package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.WebElement;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class Image extends AbstractGuiElement implements IImage {

	public Image(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	public Image(WebElement element) {
		super(element);
	}

	@Override
	public String getSource() {
		return getAttribute("src");
	}

	@Override
	public String getAlt() {
		return getAttribute("alt");
	}



}
