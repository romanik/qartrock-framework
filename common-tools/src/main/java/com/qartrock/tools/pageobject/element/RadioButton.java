package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.WebElement;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class RadioButton extends AbstractGuiElement implements IRadioButton {

	public RadioButton(WebElement element) {
		super(element);
	}

	public RadioButton(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	@Override
	public boolean isSelected() {
		return getWrappedElement().isSelected();
	}

	@Override
	public void select() {
		if (isDeselected())
			click();
	}

	@Override
	public String getValue() {
		return webElementUtils().getAttribute(getWrappedElement(), "value");
	}
	
	@Override
	public String getText() {
		return webElementUtils().getAttribute(getWrappedElement(), "data-text");
	}
}
