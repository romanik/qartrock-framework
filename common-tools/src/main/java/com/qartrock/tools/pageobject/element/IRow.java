package com.qartrock.tools.pageobject.element;

import com.qartrock.tools.genericmodel.Model;
import com.qartrock.tools.pageobject.GuiElement;

public interface IRow<T extends Model> extends GuiElement {
	T asDataModel();
}
