package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.WebElement;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class Label extends AbstractGuiElement implements ILabel {

	public Label(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	public Label(WebElement element) {
		super(element);
	}

}
