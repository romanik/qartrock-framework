package com.qartrock.tools.pageobject.element;

import com.qartrock.tools.pageobject.GuiElement;

public interface ICheckBox extends GuiElement {

	boolean isSelected();

	default boolean isDeselected() {
		return !isSelected();
	}

	void select();

	void deselect();

	void set(boolean value);
}
