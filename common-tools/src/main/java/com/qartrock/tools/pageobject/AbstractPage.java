package com.qartrock.tools.pageobject;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.google.common.base.Function;
import com.qartrock.tools.WebElementUtils;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

public abstract class AbstractPage {

	protected static final long DEFAULT_TIMEOUT_TO_WAIT = 10;

	@FindBy(css = "body")
	private WebElement body;

	private WebElementUtils webElementUtils;

	private WebDriverWait waiter;

	protected AbstractPage() {
		PageFactory.initElements(new AppiumFieldDecorator(AbstractApp.getDriver()), this);
		waitUntilLoaded();
	}

	protected WebElementUtils webElementUtils() {
		return webElementUtils != null ? webElementUtils : (webElementUtils = WebElementUtils.getInstance());
	}

	protected WebDriverWait getWebDriverWait() {
		return getWebDriverWait(DEFAULT_TIMEOUT_TO_WAIT);
	}

	protected WebDriverWait getWebDriverWait(long timeoutInSeconds) {
		if (waiter == null)
			waiter = new WebDriverWait(AbstractApp.getDriver(), DEFAULT_TIMEOUT_TO_WAIT);
		waiter.withTimeout(timeoutInSeconds, TimeUnit.SECONDS).ignoring(StaleElementReferenceException.class);

		return waiter;
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException}
	 * 
	 * @param condition
	 * @throws TimeoutException
	 */
	protected void waitUntil(Supplier<Boolean> condition) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait().until(conditionToBeTrue);
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException}
	 * 
	 * @param condition
	 * @param message
	 * @throws TimeoutException
	 */
	protected void waitUntil(Supplier<Boolean> condition, String message) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait().withMessage(message).until(conditionToBeTrue);
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException} after specified {@code timeout}
	 * 
	 * @param condition
	 * @param seconds
	 */
	protected void waitUntil(Supplier<Boolean> condition, int seconds) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait(seconds).until(conditionToBeTrue);
	}

	/**
	 * Wait for a specified condition to return {@code true} or throws
	 * {@link TimeoutException} after specified {@code timeout}
	 * 
	 * @param condition
	 * @param timeout
	 * @param message
	 */
	protected void waitUntil(Supplier<Boolean> condition, long timeout, String message) {
		Function<WebDriver, Boolean> conditionToBeTrue = (WebDriver d) -> condition.get().equals(Boolean.TRUE);
		getWebDriverWait(timeout).withMessage(message).until(conditionToBeTrue);
	}

	/**
	 * For compatibility with {@link ExpectedConditions}
	 * 
	 * @param isTrue
	 */
	protected <V> void waitUntil(Function<? super WebDriver, V> isTrue) {
		getWebDriverWait().until(isTrue);
	}

	/**
	 * For compatibility with {@link ExpectedConditions}
	 * 
	 * @param isTrue
	 */
	protected <V> void waitUntil(Function<? super WebDriver, V> isTrue, long timeout) {
		getWebDriverWait(timeout).until(isTrue);
	}

	/**
	 * Tries to wait for {@code condition} to return {@code true} but ignore
	 * {@link TimeoutException} and simply returns
	 * 
	 * @param condition
	 */
	protected void tryWaitUntil(Supplier<Boolean> condition) {
		try {
			waitUntil(condition);
		} catch (TimeoutException e) {
			// Just return control to the calling code
		}
	}

	/**
	 * For compatibility with {@link ExpectedConditions}
	 * 
	 * @param isTrue
	 */
	protected <V> void tryWaitUntil(Function<WebDriver, V> isTrue) {
		try {
			waitUntil(isTrue);
		} catch (TimeoutException e) {
			// Just return control to the calling code
		}
	}

	protected void tryWaitUntil(Supplier<Boolean> condition, int seconds) {
		try {
			waitUntil(condition, seconds);
		} catch (TimeoutException e) {
			// Just return control to the calling code
		}
	}

	protected long getDefaultWaitTimeout() {
		return DEFAULT_TIMEOUT_TO_WAIT;
	}

	// ************** ALERT HANDLING **************** //

	@Step("Accepts Alert")
	protected void acceptAlert() {
		getAlert().accept();
	}

	@Step("Dismisses Alert")
	protected void dismissAlert() {
		getAlert().dismiss();
	}

	@Attachment("Alert Text")
	@Step("Reads Alert text")
	protected String getAlertText() {
		return getAlert().getText();
	}

	private Alert getAlert() {
		return AbstractApp.getDriver().switchTo().alert();
	}

	@Step("Accepts Alert")
	protected void waitForAlertAndAccept() {
		try {
			getWebDriverWait(DEFAULT_TIMEOUT_TO_WAIT).until(ExpectedConditions.alertIsPresent());
			getAlert().accept();
		} catch (TimeoutException | NoAlertPresentException e) {
			System.out.println("NO ALERT!!!");
		}
	}

	@Step("Dismisses Alert")
	protected void waitForAlertAndDismiss() {
		try {
			getWebDriverWait(DEFAULT_TIMEOUT_TO_WAIT).until(ExpectedConditions.alertIsPresent());
			getAlert().dismiss();
		} catch (TimeoutException | NoAlertPresentException e) {
			System.out.println("NO ALERT!!!");
		}
	}

	@Step
	public void acceptAllAlerts() {
		boolean shouldWait = true;
		while (shouldWait) {
			try {
				getWebDriverWait(1).until(ExpectedConditions.alertIsPresent());
				getAlert().accept();
			} catch (TimeoutException | NoAlertPresentException e) {
				shouldWait = false;
			}
		}
	}

	@Step("Check if alert is present")
	public Boolean isAlertPresent() {
		try {
			getAlert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	protected abstract void waitUntilLoaded();
}
