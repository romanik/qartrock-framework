package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.Keys;

import com.qartrock.tools.pageobject.GuiElement;

import ru.yandex.qatools.allure.annotations.Step;

public interface ITextInput extends GuiElement {
	
	public void typeJS(String text);

	public void type(String text);

	public void clear();

	public default void clearAndType(String text) {
		clear();
		type(text);
	}

//	public void setValue(String text);

	public String getValue();
	
	public void setValue(String keysToSend);
	
	@Step
	public default void clearByBackspaces() {
		int length = this.getValue().length();
		String backspaces = "";
		for (int i = 0; i < length; i++) {
			backspaces = backspaces + Keys.BACK_SPACE;
		}
		this.type(backspaces);
	}
}
