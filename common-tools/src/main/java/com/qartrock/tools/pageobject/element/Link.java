package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.WebElement;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class Link extends AbstractGuiElement implements ILink {

	public Link(WebElement element) {
		super(element);
	}

	public Link(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	@Override
	public String getSource() {
		step(toString(), "Get 'href'");
		return getWrappedElement().getAttribute("href");
	}

}
