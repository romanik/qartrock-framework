package com.qartrock.tools.pageobject.element;

import java.util.List;

public interface IRadioButtonGroup {

	List<IRadioButton> getRadionButtons();

	void selectByValue(String value);
	
	void selectByVisibleText(String text);

	IRadioButton getSelectedRadioButton();
}
