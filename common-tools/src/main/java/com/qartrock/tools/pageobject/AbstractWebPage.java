package com.qartrock.tools.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

public abstract class AbstractWebPage extends AbstractPage {

	@FindBy(css = "body")
	private WebElement body;

	protected AbstractWebPage() {
		super();
	}

	/**
	 * Returns a string representing the current URL as seen in the address bar
	 * of browser.
	 * 
	 * @return
	 */
	@Attachment("Current URL")
	@Step("Read current URL")
	public final String getCurrentUrl() {
		return AbstractApp.getDriver().getCurrentUrl();
	}

	/**
	 * Returns the title of the current page as seen in the title bar of
	 * browser.
	 * 
	 * @return
	 */
	@Attachment("Browser Title")
	@Step("Reads title in the browser title bar")
	public final String getBrowserTitle() {
		return AbstractApp.getDriver().getTitle();
	}

	/**
	 * Returns true if and only if source of this page contains the specified
	 * string. TODO move into AbstractWebPage
	 * 
	 * @param textToFindOnPage
	 * @return true if this page source contains {@code textToFindOnPage}, false
	 *         otherwise
	 */
	@Attachment
	@Step("Checks if page contains text [{0}]")
	public final boolean contains(String textToFindOnPage) {
		String allTextOnPage = body.getText();

		return allTextOnPage.contains(textToFindOnPage);
	}

	// ************** FRAMES HANDLING **************** //

	protected void switchToFrame(int index) {
		AbstractApp.getDriver().switchTo().frame(index);
	}

	protected void switchToFrame(String nameOrId) {
		AbstractApp.getDriver().switchTo().frame(nameOrId);
	}

	protected void switchToFrame(WebElement frameElement) {
		AbstractApp.getDriver().switchTo().frame(frameElement);
	}

	protected void switchToDefaultContent() {
		AbstractApp.getDriver().switchTo().defaultContent();
	}

}
