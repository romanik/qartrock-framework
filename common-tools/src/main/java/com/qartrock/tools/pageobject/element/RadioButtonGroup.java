package com.qartrock.tools.pageobject.element;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class RadioButtonGroup extends AbstractGuiElement implements IRadioButtonGroup {

	@FindBy(css = "[type=radio]")
	private List<WebElement> radioButtons;

	public RadioButtonGroup(WebElement element) {
		super(element);
	}

	public RadioButtonGroup(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	@Override
	public void selectByValue(String value) {
		getRadionButtons().stream().filter(e -> e.getValue().equals(value)).findFirst()
				.orElseThrow(() -> new RuntimeException("There is no redio button with value '" + value + "'"))
				.select();
	}
	
	@Override
	public void selectByVisibleText(String text) {
		getRadionButtons().stream().filter(e -> e.getText().equals(text)).findFirst()
				.orElseThrow(() -> new RuntimeException("There is no redio button with text '" + text + "'"))
				.select();
	}

	@Override
	public List<IRadioButton> getRadionButtons() {
		return radioButtons.stream().map(RadioButton::new).collect(Collectors.toList());
	}

	@Override
	public IRadioButton getSelectedRadioButton() {
		Optional<IRadioButton> option = getRadionButtons().stream().filter(radio -> radio.isSelected()).findFirst();
		if (option.isPresent())
			return option.get();

		return null;
	}

}
