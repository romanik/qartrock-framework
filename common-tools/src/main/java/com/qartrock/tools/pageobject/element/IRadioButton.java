package com.qartrock.tools.pageobject.element;

public interface IRadioButton {

	String getValue();

	boolean isSelected();

	default boolean isDeselected() {
		return !isSelected();
	}

	void select();

	String getText();

}
