package com.qartrock.tools.pageobject.element;

public interface IPagination {
	IButton firstButton();

	IButton previousButton();
	
	IButton nextButton();
	
	IButton lastButton();
	
	Boolean isFirstButtonEnabled();
	
	Boolean isPreviousButtonEnabled();
	
	Boolean isNextButtonEnabled();
	
	Boolean isLastButtonEnabled();
}
