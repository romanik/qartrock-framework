package com.qartrock.tools.pageobject;

import org.openqa.selenium.OutputType;

public interface GuiElement {
	public boolean exists();

	public boolean isShown();

	public default boolean isHidden() {
		return !isShown();
	}

	public boolean isEnabled();

	public default boolean isDisabled() {
		return !isEnabled();
	}

	public void click();
	
	public void doubleClick();

	public String getText();

	public <X> X getScreenshotAs(OutputType<X> target);
	
	public String getAttribute(String attributeName);
	
	public void scrollIntoView();
	
	public void hoverOver();

}
