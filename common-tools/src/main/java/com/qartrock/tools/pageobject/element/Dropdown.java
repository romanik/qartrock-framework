package com.qartrock.tools.pageobject.element;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.qartrock.tools.pageobject.AbstractGuiElement;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

public class Dropdown extends AbstractGuiElement implements IDropdown {

	public Dropdown(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	public Dropdown(WebElement wrappedElement) {
		super(wrappedElement);
	}

	@Override
	public String getTextValueOfFirstOption() {
		return null;
	}

	private Select getSelect() {
		return new Select(getWrappedElement());
	}

	public boolean isMultiple() {
		return getSelect().isMultiple();
	}

	public List<WebElement> getOptions() {
		return getSelect().getOptions();
	}

	public List<WebElement> getAllSelectedOptions() {
		return getSelect().getAllSelectedOptions();
	}

	public WebElement getFirstSelectedOption() {
		return getSelect().getFirstSelectedOption();
	}

	@Override
	public String getSelectedOption() {
		return webElementUtils().getTextFrom(getFirstSelectedOption());
	}

	public boolean hasSelectedOption() {
		return getOptions().stream().anyMatch(WebElement::isSelected);
	}

	@Step("Select Option by text <{0}>")
	public void selectByVisibleText(String text) {
		getSelect().selectByVisibleText(text);
	}

	@Step("Select Option by index <{0}>")
	public void selectByIndex(int index) {
		getSelect().selectByIndex(index);
	}

	public void selectByValue(String value) {
		getSelect().selectByValue(value);
	}

	@Step("Deselect all options")
	public void deselectAll() {
		getSelect().deselectAll();
	}

	public void deselectByValue(String value) {
		getSelect().deselectByValue(value);
	}

	@Step("Deselect option by index <{0}>")
	public void deselectByIndex(int index) {
		getSelect().deselectByIndex(index);
	}

	@Step("Deselect option by text <{0}>")
	public void deselectByVisibleText(String text) {
		getSelect().deselectByVisibleText(text);
	}

	@Attachment("Options")
	@Step("Read all options")
	public List<String> getAllOptions() {
		return getOptions().stream().map(option -> option.getText()).collect(Collectors.toList());
	}

	@Override
	public void selectByTitle(String text) {
		throw new RuntimeException("Not implemented");
	}

}
