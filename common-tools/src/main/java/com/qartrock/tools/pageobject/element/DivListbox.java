package com.qartrock.tools.pageobject.element;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qartrock.tools.pageobject.AbstractGuiElement;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Describes dropdown list options element implemented through list of
 * <div><div/>.
 * 
 * Created by ovol on 2/13/2018
 */
public class DivListbox extends AbstractGuiElement implements IDropdown {
	@FindBy(css = "div[class^= dropdown]")
	private List<WebElement> listOption;

	public DivListbox(WebElement element) {
		super(element);
	}

	public DivListbox(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	@Step("Select Option by text <{0}>")
	public void selectByVisibleText(String text) {
		step(toString(), "Select by text: <" + text + ">");
		listOption.stream().filter(el -> el.getText().equalsIgnoreCase(text)).findAny()
				.orElseThrow(() -> new NoSuchElementException("\nNo such option in dropdown: " + text + "\n")).click();
	}

	public void selectByTitle(String text) {
		throw new RuntimeException("Not implemented exception");
	}

	public void selectByIndex(int index) {
		index = index - 1;
		listOption.get(index).click();
	}

	public String getTextValueOfFirstOption() {
		return listOption.get(0).getText();
	}

	@Override
	public String getSelectedOption() {
		throw new UnsupportedOperationException("Not implemented yet");
	}

	@Override
	public void selectByValue(String value) {
		throw new UnsupportedOperationException("Not implemented yet");
	}

	@Override
	@Step("Read all options")
	@Attachment("All options")
	public List<String> getAllOptions() {
		return listOption.stream().map(WebElement::getText).collect(Collectors.toList());
	}
}
