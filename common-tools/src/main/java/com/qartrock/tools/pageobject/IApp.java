package com.qartrock.tools.pageobject;

import java.io.File;

public interface IApp {

	public File getScreenshot();

	/**
	 * Does nothing with the application for a time specified by {@code aWhile}
	 * in milliseconds.
	 * 
	 * @param aWhile
	 *            time to hold in milliseconds
	 */
	public default void holdOnFor(long aWhile) {
		try {
			Thread.sleep(aWhile);
		} catch (InterruptedException e) {
			// Just in case
			e.printStackTrace();
		}
	}
}
