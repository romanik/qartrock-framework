package com.qartrock.tools.pageobject.element;

import java.util.List;
import java.util.Set;

import com.qartrock.tools.genericmodel.Model;

public interface IMultiTable<RowModel extends Model, Row extends IRow<RowModel>> extends ITable<RowModel, Row> {

	List<RowModel> getVisibleModelList();

	boolean isPaginationShown();

	IPagination pagination();
	
	boolean softContains(RowModel row);
	
	<T extends Enum<T>> boolean contains(RowModel row, Set<T> fieldsToIterate);

}
