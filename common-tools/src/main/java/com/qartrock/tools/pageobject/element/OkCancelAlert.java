package com.qartrock.tools.pageobject.element;

/**
 * Describes a notification alert with a message and OK and Cancel buttons
 */
public interface OkCancelAlert extends OkAlert {

	public IButton cancelButton();

	public void cancel();
}
