package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class Pagination extends AbstractGuiElement implements IPagination {

	@FindBy(css = "[class='page-item active']")
	private WebElement activeTab;
	@FindBy(css = ".first")
	private WebElement first;
	@FindBy(css = ".prev")
	private WebElement previous;
	@FindBy(css = ".next")
	private WebElement next;
	@FindBy(css = ".last")
	private WebElement last;
	@FindBy(css = ".first>a")
	private WebElement firstButton;
	@FindBy(css = ".prev>a")
	private WebElement previousButton;
	@FindBy(css = ".next>a")
	private WebElement nextButton;
	@FindBy(css = ".last>a")
	private WebElement lastButton;
	
	public Pagination(WebElement element) {
		super(element);
	}

	public Pagination(WebElement element, String name) {
		super(element, name);
	}
	
	@Override
	public IButton firstButton() {
		return new Button(firstButton, "First button");
	}
	
	@Override
	public IButton previousButton() {
		return new Button(previousButton, "Previous button");
	}
	
	@Override
	public IButton nextButton() {
		return new Button(nextButton, "Next button");
	}
	
	@Override
	public IButton lastButton() {
		return new Button(lastButton, "Last button");
	}
	
	@Override
	public Boolean isFirstButtonEnabled() {
		return !first.getAttribute("class").contains("disabled");
	}
	
	@Override
	public Boolean isPreviousButtonEnabled() {
		return !previous.getAttribute("class").contains("disabled");
	}
	
	@Override
	public Boolean isNextButtonEnabled() {
		return !next.getAttribute("class").contains("disabled");
	}
	
	@Override
	public Boolean isLastButtonEnabled() {
		return !last.getAttribute("class").contains("disabled");
	}
	
}
