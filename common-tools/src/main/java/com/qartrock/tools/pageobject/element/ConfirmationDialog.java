package com.qartrock.tools.pageobject.element;

import com.qartrock.tools.pageobject.GuiElement;

public interface ConfirmationDialog extends GuiElement {

	public IButton cancelButton();

	public IButton confirmButton();

	public ILabel message();

	public void cancel();

	public void confirm();

	public String getMessage();
	
	public String getTitle();
}
