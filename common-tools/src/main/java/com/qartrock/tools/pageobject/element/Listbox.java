package com.qartrock.tools.pageobject.element;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qartrock.tools.pageobject.AbstractGuiElement;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

public class Listbox extends AbstractGuiElement implements IDropdown {

	@FindBy(css = "li")
	private List<WebElement> listOption;

	public Listbox(WebElement element) {
		super(element);
	}

	public Listbox(WebElement element, String name) {
		super(element, name);
	}

	@Step("Select Option by text <{0}>")
	public void selectByVisibleText(String text) {
		step(toString(), "Select by text: <" + text + ">");
		listOption.stream().filter(el -> el.getText().contains(text)).findAny()
				.orElseThrow(() -> new NoSuchElementException("\nNo such option in dropdown: " + text + "\n")).click();
	}

	public void selectByTitle(String text) {
		throw new RuntimeException("Not implemented exception");
	}

	public void selectByIndex(int index) {
		index = index - 1;
		listOption.get(index).click();
	}

	public String getTextValueOfFirstOption() {
		return listOption.get(0).getText();
	}

	@Override
	public String getSelectedOption() {
		throw new UnsupportedOperationException("\nNot implemented yet\n");
	}

	@Override
	public void selectByValue(String value) {
		throw new UnsupportedOperationException("\nNot implemented yet\n");
	}

	@Override
	@Step("Read all options")
	@Attachment("All options")
	public List<String> getAllOptions() {
		return listOption.stream().map(WebElement::getText).collect(Collectors.toList());
	}

}
