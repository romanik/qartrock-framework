package com.qartrock.tools.pageobject.element;

import org.apache.commons.lang3.StringUtils;

import com.qartrock.tools.pageobject.GuiElement;

public interface SearchInput extends GuiElement {

	public ITextInput textField();

	public IImage magnifyingGlassIcon();

	public IImage xIcon();

	public default void type(String text) {
		textField().type(text);
	}

	public void clear();

	public default void clearAndType(String text) {
		clear();
		type(text);
	}

	public default void clearByXIcon() {
		xIcon().click();
	}

	public default void clearByXIconAndType(String text) {
		clearByXIcon();
		type(text);
	}
	
	public String getValue();
	
	public default boolean isEmpty() {
		return StringUtils.isEmpty(getValue());
	}
}
