package com.qartrock.tools.pageobject.element;

import java.util.Optional;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class Input extends AbstractGuiElement implements ITextInput {

	public Input(WebElement element) {
		super(element);
	}

	public Input(WebElement element, String name) {
		super(element, name);
	}
	
	@Override
	public void typeJS(String text) {
		step(toString(), "Type text using JS [" + text + "]");
		JavascriptExecutor getJavascriptExecutor = (JavascriptExecutor) getWrappedDriver();
		getJavascriptExecutor.executeScript("arguments[0].innerHTML=\"" + text + "\"", getWrappedElement());
	}

	@Override
	public void type(String text) {
		step(toString(), "Type text [" + text + "]");
		getWrappedElement().sendKeys(text);
	}

	@Override
	public void clear() {
		step(toString(), "Clear");
		getWrappedElement().clear();
	}

	@Override
	public void clearAndType(String text) {
		step(toString(), "Clear and type");
		ITextInput.super.clearAndType(text);
	}

	@Override
	public String getValue() {
		step(toString(), "Read value");
		return Optional.of(webElementUtils().getValueOf(getWrappedElement())).orElse("");
	}
	
	/**
	 * Clears the value of web element and sends keys to it.
	 * 
	 * @param element
	 * @param keysToSend
	 */
	protected void setValue(WebElement element, String keysToSend) {
		webElementUtils().clearAndSendKeysTo(element, keysToSend);
		try {
			getWebDriverWait().until(ExpectedConditions.attributeToBe(element, "value", keysToSend));
		} catch (TimeoutException e) {
			System.out.println("Value is " + element.getAttribute("value"));
			throw new TimeoutException("Value" + keysToSend + " was not set to input field");
		}
	}

	@Override
	public void setValue(String text) {
		step(toString(), "Type text [" + text + "]");
		setValue(getWrappedElement(), text);
	}

}
