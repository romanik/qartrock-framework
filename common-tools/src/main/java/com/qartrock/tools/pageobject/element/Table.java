package com.qartrock.tools.pageobject.element;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qartrock.tools.genericmodel.Model;
import com.qartrock.tools.pageobject.AbstractGuiElement;

import ru.yandex.qatools.allure.annotations.Step;

public abstract class Table<RowModel extends Model, Row extends IRow<RowModel>> extends AbstractGuiElement implements ITable<RowModel, Row> {

	@FindBy(css = ".DivSubHeaderMain #_ApolloSubHeaderTitleDiv")
	private WebElement title;

	@FindBy(xpath = "./tbody/tr[td]|./*/tbody/tr[td]|./*/*/tbody/tr[td]|./*/*/*/tbody/tr[td]|./*/*/*/*/tbody/tr[td]|./*/*/*/*/*/tbody/tr[td]")
//	@FindBy(css = ">tbody>tr")
	private List<WebElement> rows;

	public Table(WebElement element) {
		super(element);
	}

	public Table(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	@Override
	@Step("Checks if title is shown")
	public boolean isTitleShown() {
		return webElementUtils().isElementShown(title);
	}

	@Override
	@Step("Read table title")
	public String getTitle() {
		if (!isTitleShown())
			throw new IllegalStateException(toString() + ": Title should be shown!\n");
		return webElementUtils().getTextFrom(title);
	}

	@Override
	public List<Row> getRows() {
		return rows.stream().map(this::getInstance).collect(Collectors.toList());
	}

	@Override
	public List<RowModel> getModelList() {
		return getRows().stream().map(Row::asDataModel).collect(Collectors.toList());
	}

	@Override
	public List<RowModel> getUniqueModelList() {
		return getRows().stream().map(Row::asDataModel).distinct().collect(Collectors.toList());
	}

	@Override
	@Step("Checks if table contains data: {0}")
	public boolean contains(RowModel row) {
		return getModelList().contains(row);
	}

	@Override
	@Step("Get row from the table by index [{0}]")
	public Row getRow(int index) {
		return getInstance(rows.get(index));
	}

	@Override
	@Step("Get row from the table by data: {0}")
	public Row getRow(RowModel row) {
		int index = getModelList().indexOf(row);

		if (index == -1)
			throw new NoSuchElementException("\nThere is no such data " + row + " row in the table.\n");

		return getRow(index);
	}

	protected abstract Row getInstance(WebElement rowElement);
}
