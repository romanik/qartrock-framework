package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.WebElement;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class Button extends AbstractGuiElement implements IButton {

	public Button(WebElement element) {
		super(element);
	}

	public Button(WebElement element, String name) {
		super(element, name);
	}
}
