package com.qartrock.tools.pageobject.element;

import java.util.List;

import com.qartrock.tools.pageobject.GuiElement;

public interface INode<N extends INode<N>> extends GuiElement {

	public String getName();

	public boolean isExpanded();

	public List<N> getChildNodes();

	public List<String> getChildNodesNames();

	public N getChildNode(String childName);

	public boolean contains(String child);

	public N expand();

	public N collapse();

	public N select();

	public String getTooltip();

}
