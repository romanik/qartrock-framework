package com.qartrock.tools.pageobject;

import java.io.File;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.qartrock.tools.browser.Browser;

import ru.yandex.qatools.allure.annotations.Step;

public abstract class AbstractApp implements IApp {

	private static ThreadLocal<Browser> browser = new ThreadLocal<Browser>() {
		@Override
		protected Browser initialValue() {
			return new Browser();
		}
	};

	public static WebDriver getDriver() {
		return browser.get().getDriver();
	}

	public static JavascriptExecutor getJavaScriptExecutor() {
		return JavascriptExecutor.class.cast(getDriver());
	}

	@Step("Close the browser")
	public static void close() {
		browser.get().quit();
	}

	@Step("Get current URL")
	public static String getCurrentUrl() {
		return getDriver().getCurrentUrl();
	}

	public static boolean canTakeScreenshot() {
		return browser.get().isOpened();
	}

	protected AbstractApp() {
	}

	public static boolean hasConsoleLogs() {
		return browser.get().hasLoggedErrors();
	}

	public static String getConsoleLogs() {
		return String.valueOf(browser.get().getLoggedErrors());
	}

	@Override
	public File getScreenshot() {
		if (canTakeScreenshot()) {
			return TakesScreenshot.class.cast(getDriver()).getScreenshotAs(OutputType.FILE);
		}
		return null;
	}

	public void clearAllCookies() {
		if (browser.get().isOpened()) {
			getDriver().manage().deleteAllCookies();
		}
	}

	public void switchToDefaultContent() {
		getDriver().switchTo().defaultContent();
	}

	public static void switchToTab(int tabPosition) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Set<String> tabs = getDriver().getWindowHandles();
		String switchTab = null;
		int count = 0;
		for (String tab : tabs) {
			count++;
			if (count == tabPosition) {
				switchTab = tab;
			}
		}
		getDriver().switchTo().window(switchTab);
	}

	@Step("{0}")
	protected void logStep(String step) {
	}

}
