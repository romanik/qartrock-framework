package com.qartrock.tools.pageobject.element;

import org.openqa.selenium.WebElement;

import com.qartrock.tools.pageobject.AbstractGuiElement;

public class CheckBox extends AbstractGuiElement implements ICheckBox {

	public CheckBox(WebElement element) {
		super(element);
	}

	public CheckBox(WebElement wrappedElement, String name) {
		super(wrappedElement, name);
	}

	@Override
	public boolean isSelected() {
		return getWrappedElement().isSelected();
	}

	@Override
	public void select() {
		if (isDeselected())
			click();
	}

	@Override
	public void deselect() {
		if (isSelected())
			click();
	}

	@Override
	public void set(boolean value) {
		if (value) {
			select();
		} else {
			deselect();
		}
	}

}
