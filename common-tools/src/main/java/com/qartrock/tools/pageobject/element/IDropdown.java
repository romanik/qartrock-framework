package com.qartrock.tools.pageobject.element;

import java.util.List;

import com.qartrock.tools.pageobject.GuiElement;

public interface IDropdown extends GuiElement {
	void selectByTitle(String text);
	
	void selectByValue(String value);

	void selectByVisibleText(String text);

	void selectByIndex(int index);

	String getTextValueOfFirstOption();

	String getSelectedOption();

	List<String> getAllOptions();
}
