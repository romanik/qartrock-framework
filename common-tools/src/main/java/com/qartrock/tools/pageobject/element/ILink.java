package com.qartrock.tools.pageobject.element;

import com.qartrock.tools.pageobject.GuiElement;

public interface ILink extends GuiElement{
	public String getSource();
}
