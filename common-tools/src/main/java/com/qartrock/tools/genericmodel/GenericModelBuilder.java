package com.qartrock.tools.genericmodel;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class GenericModelBuilder<UiFields extends Enum<UiFields>> {
	private Map<UiFields, Object> fields = new HashMap<UiFields, Object>();

	public GenericModel<UiFields> build() {
		return new GenericModel<>(fields);
	}

	public <T extends GenericModel<UiFields>> T build(Class<T> clazz) {
		try {
			return clazz.getConstructor(Map.class).newInstance(fields);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("\nCould not build GenericModel object\n", e);
		}
	}

	public GenericModelBuilder<UiFields> setField(UiFields field, Object value) {
		this.fields.put(field, value);
		return this;
	}
}
