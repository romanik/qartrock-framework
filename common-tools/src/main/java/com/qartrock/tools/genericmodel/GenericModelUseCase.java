package com.qartrock.tools.genericmodel;
import static com.qartrock.tools.genericmodel.ProfileDetailsField.*;

import java.util.Map;

public class GenericModelUseCase {

	public static void main(String[] args) {
		GenericModelBuilder<ProfileDetailsField> builder = GenericModel.builder();

		GenericModel<ProfileDetailsField> model = builder.setField(ProfileDetailsField.FULL_NAME, "Roma Romanik")
				.setField(ProfileDetailsField.JOB_TITLE, "physisist")
				.setField(ProfileDetailsField.MOBILE_NUMBER, "1111").build();

		System.out.println(model);

		GenericModel<ProfileDetailsField> model2 = GenericModel.<ProfileDetailsField>builder()
				.setField(ProfileDetailsField.FULL_NAME, "John Doe").build();
		System.out.println(model2);

		CustomModel custom = CustomModel.<ProfileDetailsField>builder().setField(FULL_NAME, "John Doe Jr.")
				.build(CustomModel.class);
		System.out.println(custom);
	}
}

class CustomModel extends GenericModel<ProfileDetailsField> {

	public CustomModel(Map<ProfileDetailsField, Object> fields) {
		super(fields);
	}

}

enum ProfileDetailsField {
	FULL_NAME("Full Name"),

	JOB_TITLE("Job Title"),

	MOBILE_NUMBER("Mobile Number"),

	EMAIL_ADDRESS("Email Address"),

	USER_LINK("User Link"),

	TIMEZONE("Timezone"),

	TEAM_ASSOCIATION("Team Association"),

	ALLOW_POST_SIGN_EDITING("Allow Post-sign Editing"),

	SHOW_IN_DISCHARGE("Shown In Discharge"),

	ALLOWED_APPLICATIONS("Allowed Applications"),

	LETTER_SIGNOFF("Letter Signoff"),

	DEFAULT_LANDING_PAGE("Default Landing Page"),

	USER_ROLE("User Role");

	private final String label;

	private ProfileDetailsField(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return this.label;
	}
}
