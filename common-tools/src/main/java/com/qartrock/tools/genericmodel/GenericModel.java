package com.qartrock.tools.genericmodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.qartrock.exeptions.DataTypeException;

public class GenericModel<UiField extends Enum<UiField>> extends Model {

	private final Map<UiField, Object> fields;

	public GenericModel(Map<UiField, Object> fields) {
		this.fields = Collections.unmodifiableMap(fields);
	}

	public Object get(UiField field) {
		return fields.get(field);
	}

	public String getString(UiField field) {
		if (fields.get(field) instanceof String) {
			return (String) fields.get(field);
		}
		throw new DataTypeException("Object [" + fields.get(field) + "] is not instance of String class");
	}

	public Integer getInteger(UiField field) {
		if (fields.get(field) instanceof Integer) {
			return (Integer) fields.get(field);
		}
		throw new DataTypeException("Object [" + fields.get(field) + "] is not instance of Integer class");
	}

	public Double getDouble(UiField field) {
		if (fields.get(field) instanceof Double) {
			return (Double) fields.get(field);
		}
		throw new DataTypeException("Object [" + fields.get(field) + "] is not instance of Double class");
	}

	public Boolean getBoolean(UiField field) {
		if (fields.get(field) instanceof Boolean) {
			return (Boolean) fields.get(field);
		}
		throw new DataTypeException("Object [" + fields.get(field) + "] is not instance of Boolean class");
	}

	public LocalDate getLocalDate(UiField field) {
		if (fields.get(field) instanceof LocalDate) {
			return (LocalDate) fields.get(field);
		}
		throw new DataTypeException("Object [" + fields.get(field) + "] is not an instance of LocalDate class");
	}

	public LocalDateTime getLocalDateTime(UiField field) {
		if (fields.get(field) instanceof LocalDateTime) {
			return (LocalDateTime) fields.get(field);
		}
		throw new DataTypeException("Object [" + fields.get(field) + "] is not an instance of LocalDateTime class");
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getList(UiField field) {
		Object value = fields.get(field);
		if (value instanceof List) {
			return ((List<T>) value);
		}
		throw new DataTypeException("Object [" + fields.get(field) + "] is not an instance of java.util.List");
	}

	public Set<UiField> includedFields() {
		return fields.keySet();
	}
	
	public boolean containsField(UiField field) {
		return fields.containsKey(field);
	}

	public static <UF extends Enum<UF>> GenericModelBuilder<UF> builder() {
		return new GenericModelBuilder<>();
	}

	public static <UF extends Enum<UF>> GenericModelBuilder<UF> builder(Class<UF> clazz) {
		return new GenericModelBuilder<>();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fields == null) ? 0 : fields.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenericModel<?> other = GenericModel.class.cast(obj);
		if (fields == null) {
			if (other.fields != null)
				return false;
		} else if (!fields.equals(other.fields))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[fields=" + fields + "]";
	}

}
