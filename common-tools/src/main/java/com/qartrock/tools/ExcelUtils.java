package com.qartrock.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javafunk.excelparser.SheetParser;

public class ExcelUtils {
	
	private static XSSFSheet getXLSXSheet(File excelFile){
		XSSFSheet sheet = null;
		try {
			InputStream inputStream = new FileInputStream(excelFile);
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			sheet = workbook.getSheetAt(0);
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sheet;
	}
	
	@SuppressWarnings("deprecation")
	public static <T> List<T> getEntityList(File file, Class<T> clazz) {
		XSSFSheet sheet = getXLSXSheet(file);
		SheetParser parser = new SheetParser();
		return parser.createEntity(sheet, new String(), clazz);
	}

}
