package com.qartrock.tools;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Dimension;

import com.qartrock.tools.browser.DriverType;

/**
 * 
 * @author rromanik
 *
 */
public class ConfigurationManager {

	private static final String MOBILE_PLATFORM_VARIABLE = "mobile";
	private static final String BROWSER_SYSTEM_VARIABLE = "browser";

	private static final String REMOTE_FLAG_VARIABLE = "remote";

	private static final String ENVIRONMENT_SYSTEM_VARIABLE = "env";
	private static final String HOST = "host";

	private static final String VERSION = "version";

	private static final String LOGIN_VARIABLE = "login";
	private static final String PASSWORD_VARIABLE = "pass";
	private static final String ROLE_VARIABLE = "role";

	private static final String THREAD_COUNT_SYSTEM_VARIABLE = "threads";

	private static final String TEST_APPLICATION_SYSTEM_VARIABLE = "testapp";

	private static final String TAKE_SCREENSHOT_ON_SUCCESS = "takeScreenshotOnSuccess";
	private static final String TAKE_SCREENSHOT = "takeScreenshot";

	private static final String RESOLUTION = "resolution";

	private static final String NO_CLEAN_UP = "noclean";

	// A utility class
	private ConfigurationManager() {
	}

	/**
	 * Returns the value of {@link #BROWSER_SYSTEM_VARIABLE} system property.
	 * 
	 * @return value of {@value #BROWSER_SYSTEM_VARIABLE} system property.
	 */
	public static String getBrowserName() {
		String browserName = System.getProperty(BROWSER_SYSTEM_VARIABLE);

		return browserName;
	}

	/**
	 * Returns a {@link DriverType} based on the value of
	 * {@value #BROWSER_SYSTEM_VARIABLE} system property, if the latter is
	 * neither null nor empty string, or DriverType.CHROME otherwise.
	 * 
	 * @return {@link DriverType} based on the value of
	 *         {@value #BROWSER_SYSTEM_VARIABLE} system property, if the latter
	 *         is neither null nor empty string, or {@link DriverType}
	 *         representing Chrome browser.
	 * 
	 * @throws IllegalArgumentException
	 *             if no {@link DriverType} can be deduced from
	 *             {@value #BROWSER_SYSTEM_VARIABLE} system property.
	 */
	public static DriverType getDriverType() {
		DriverType driverType = DriverType.CHROME;
		String browserName = getBrowserName();

		if (!StringUtils.isEmpty(browserName)) {
			driverType = DriverType.from(browserName);
		}

		System.out.println("BROWSER: " + driverType);
		return driverType;
	}

	public static String getEnvironment() {
		String defaultEnvironment = System.getProperty(ENVIRONMENT_SYSTEM_VARIABLE);
		defaultEnvironment = (defaultEnvironment != null) ? defaultEnvironment.toLowerCase() : "UAT";
		System.out.println("ENVIRONMENT: " + defaultEnvironment);

		return defaultEnvironment;
	}

	/**
	 * Returns the value of {@value #HOST} system property, if the latter is
	 * neither null nor empty string, or empty string {@value StringUtils#EMPTY}
	 * otherwise
	 * 
	 * @return the value of {@value #HOST} system property if it is not
	 *         {@code null}, or empty string otherwise
	 */
	public static String getHost() {
		String host = System.getProperty(HOST);
		if (StringUtils.isEmpty(host))
			return StringUtils.EMPTY;
		return host;
	}

	public static String getVersion() {
		String version = System.getProperty(VERSION);
		if (StringUtils.isEmpty(version))
			return StringUtils.EMPTY;
		return version;
	}

	/**
	 * Returns a {@link Platform} based on the value of
	 * {@value #MOBILE_PLATFORM_VARIABLE} system property or the
	 * {@link Platform} representing the underlying OS, based on the value
	 * returned by {@code System.getProperty("os.name")}, if the value of
	 * {@value #MOBILE_PLATFORM_VARIABLE} property is either {@code null} or
	 * empty string.
	 * 
	 * @return mobile {@link Platform} for {@value #MOBILE_PLATFORM_VARIABLE}
	 *         system property value, if the latter is not {@code null},
	 *         underlying OS {@link Platform} otherwise
	 * @throws IllegalArgumentException
	 *             If no {@link Platform} name can be deduced from
	 *             {@value #MOBILE_PLATFORM_VARIABLE} system property
	 */
	public static Platform getPlatformOrCurrentOsName() {
		Platform platform = Platform.THIS_OS;
		String platformName = System.getProperty(MOBILE_PLATFORM_VARIABLE);

		if (!StringUtils.isEmpty(platformName)) {
			platform = Platform.from(platformName);
		}

		System.out.println("PLATFORM: " + platform);
		return platform;
	}

	/**
	 * Returns a flag value to indicate if tests should be started on Remote
	 * server or not.
	 * 
	 * @return true if the {@code remote} system property value is set to true,
	 *         false otherwise
	 */
	public static boolean isRemote() {
		String remoteValue = System.getProperty(REMOTE_FLAG_VARIABLE);

		return remoteValue != null ? Boolean.parseBoolean(remoteValue) : false;
	}

	/**
	 * Returns a flag value to indicate if screenshots should be taken on
	 * success tests.
	 * 
	 * @return true if the {@code takeScreenshotOnSuccess} system property value
	 *         is set to true, false otherwise
	 */
	public static boolean takeScreenshotOnSuccess() {
		String takeScreenValue = System.getProperty(TAKE_SCREENSHOT_ON_SUCCESS);

		return takeScreenValue != null ? Boolean.parseBoolean(takeScreenValue) : takeScreenshot();
	}

	public static boolean takeScreenshot() {
		String takeScreenshotValue = System.getProperty(TAKE_SCREENSHOT);

		return takeScreenshotValue != null ? Boolean.parseBoolean(takeScreenshotValue) : true;
	}

	/**
	 * Returns a flag value to indicate if clean up should be performed, e.g. a
	 * Suite clean up.
	 * 
	 * @return true if the {@code noclean} system property value is not set, or
	 *         set to false (ignoring case); returns false otherwise
	 */
	public static boolean cleanUp() {
		String noCleanUp = System.getProperty(NO_CLEAN_UP);

		if (noCleanUp != null && "true".equals(noCleanUp.toLowerCase()))
			return false;
		return true;
	}

	/**
	 * Returns a {@link Dimension} object representing screen resolution, e.g.
	 * '1920x1080'. It is based on the {@value #RESOLUTION} parameter.
	 * 
	 * @return resolution
	 */
	public static Dimension getResolution() {
		String resolution = System.getProperty(RESOLUTION);
		if (resolution == null)
			return null;

		String resolutionRegex = "\\d{3,4}x\\d{3,4}";
		if (!resolution.matches(resolutionRegex))
			throw new IllegalArgumentException("Provided parameter 'resolution'=" + resolution
					+ " is incorrect.\nResolution should be in format: DDDDxDDD, e.g. 1920x1080\n");

		String[] dimensions = resolution.split("x");
		return new Dimension(Integer.parseInt(dimensions[0]), Integer.parseInt(dimensions[1]));
	}

	/**
	 * Returns a positive number to represent the thread count to run tests in
	 * parallel. If such a value is not provided as a command-line parameter,
	 * returns -1.
	 * 
	 * @return number of threads to run tests or -1 if such a value is not
	 *         provided from the command-line
	 * @throws IllegalArgumentException
	 *             if provided command-line parameter value is not a positive
	 *             integer number
	 */
	public static int getThreadCount() {
		String threadCount = System.getProperty(THREAD_COUNT_SYSTEM_VARIABLE);
		if (threadCount == null)
			return -1;

		if (!StringUtils.isNumeric(threadCount))
			throw new IllegalArgumentException("\nValue of the command-line parameter '" + THREAD_COUNT_SYSTEM_VARIABLE
					+ "' must be a positive integer number\n");

		return Integer.parseInt(threadCount);
	}

	public static String getTestAppName() {
		return System.getProperty(TEST_APPLICATION_SYSTEM_VARIABLE);
	}

	public static String getLogin() {
		return System.getProperty(LOGIN_VARIABLE);
	}

	public static String getPassword() {
		return System.getProperty(PASSWORD_VARIABLE);
	}

	public static String getRole() {
		return System.getProperty(ROLE_VARIABLE);
	}
}