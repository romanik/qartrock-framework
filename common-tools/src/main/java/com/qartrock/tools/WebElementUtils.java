package com.qartrock.tools;

import static io.appium.java_client.pagefactory.utils.WebDriverUnpackUtility.unpackWebDriverFromSearchContext;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.pagefactory.utils.WebDriverUnpackUtility;

/**
 * This class contains utility methods to work with WebElement objects
 *
 */
public class WebElementUtils {

	protected static final long DEFAULT_TIMEOUT_TO_CLICK = 10;
	protected static final long DEFAULT_TIMEOUT_TO_WAIT = 10;

	private WebElementUtils() {
	}

	private WebDriverWait webDriverWait(WebElement element) {
		return webDriverWait(element, DEFAULT_TIMEOUT_TO_WAIT);
	}

	private WebDriverWait webDriverWait(WebElement element, long timeInSeconds) {
		WebDriverWait waiter = new WebDriverWait(unpackWebDriverFromSearchContext(element), timeInSeconds);
		waiter.ignoring(StaleElementReferenceException.class);
		return waiter;
	}

	public static WebElementUtils getInstance() {
		return new WebElementUtils();
	}

	private Actions getActions(WebElement element) {
		Actions actions = new Actions(unpackWebDriverFromSearchContext(element));
		return actions;
	}

	/**
	 * Performs double click on element
	 * 
	 * @param element
	 */
	public void doubleClickOn(WebElement element) {
		try {
			getActions(element).doubleClick(element).build().perform();
		} catch (WebDriverException e) {
			scrollIntoView(element);
			getActions(element).doubleClick(element).build().perform();
		}
	}

	/**
	 * Clicks on element and hold it
	 * 
	 * @param element
	 */
	public void clickAndHoldOn(WebElement element) {

		scrollIntoView(element);
		getActions(element).clickAndHold(element).build().perform();
	}

	/**
	 * Move mouse arrow to element
	 * 
	 * @param element
	 */
	public void moveTo(WebElement element) {
		scrollIntoView(element);
		getActions(element).moveToElement(element).build().perform();
	}

	/**
	 * Drag and drop WebElement to target WebElement
	 * 
	 * @param element
	 * @param target
	 */
	public void dragAndDrop(WebElement element, WebElement target) {
		scrollIntoView(element);
		getActions(element).dragAndDrop(element, target).build().perform();
	}

	/**
	 * Drag and drop WebElement by coordinates
	 * 
	 * @param element
	 * @param xOffset
	 * @param yOffset
	 */
	public void dragAnDropBy(WebElement element, int xOffset, int yOffset) {
		scrollIntoView(element);
		getActions(element).dragAndDropBy(element, xOffset, yOffset).build().perform();
	}

	/**
	 * Returns true if and only if element is present in DOM and is displayed.
	 * 
	 * @param element
	 * @return true if {@code element} is visible
	 */
	public boolean isElementShown(WebElement element) {
		try {
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		} catch (StaleElementReferenceException e) {
			return isElementShown0(element);
		}
	}

	private boolean isElementShown0(WebElement element) {
		try {
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * Checks if {@code element} is enabled (active) and returns true, if enabled,
	 * or false otherwise.
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementEnabled(WebElement element) {
		return element.isEnabled();
	}

	/**
	 * Performs click on element.
	 * 
	 * @param element
	 */
	public void clickOn(WebElement element) {
		try {
			element.click();
		} catch (WebDriverException e) {
			scrollIntoView(element);
			element.click();
		}
	}

	/**
	 * Clicks on web element with the help of JavaScript
	 * 
	 * @param element
	 */
	public void clickJS(WebElement element) {
		getJavascriptExecutor(element).executeScript("arguments[0].click();", element);
	}

	/**
	 * Waits for element to be clickable and then clicks on it.
	 * 
	 * @param element
	 *            WebElement to click on
	 * @param timeInSeconds
	 *            maximum time in seconds to wait
	 */
	public void waitAndClick(WebElement element, long timeInSeconds) {
		webDriverWait(element, timeInSeconds).until(ExpectedConditions.elementToBeClickable(element)).click();
	}

	/**
	 * Waits for element to be clickable and then clicks on it. Uses
	 * <code>DEFAULT_TIMEOUT_TO_CLICK</code> as maximum time to wait.
	 * 
	 * @param element
	 */
	public void waitAndClick(WebElement element) {
		waitAndClick(element, DEFAULT_TIMEOUT_TO_CLICK);
	}

	/**
	 * Sends keys to web element.
	 * 
	 * @param element
	 * @param keysToSend
	 */
	public void sendKeysTo(WebElement element, String keysToSend) {
		element.sendKeys(keysToSend);
	}

	/**
	 * Clears the value of web element. Uses Selenium tools
	 * 
	 * @param element
	 */
	public void clear(WebElement element) {
		element.clear();
	}

	/**
	 * Clears the value of web element and sends keys to it.
	 * 
	 * @param element
	 * @param keysToSend
	 */
	public void clearAndSendKeysTo(WebElement element, String keysToSend) {
		clear(element);
		sendKeysTo(element, keysToSend);
	}

	/**
	 * Clears {@code element} and sends keys {@code valueToSet} to it. Waits until
	 * element's {@code value} attribute is equal to {@code valueToSet}
	 * 
	 * @param element
	 * @param valueToSet
	 */
	public void setValue(WebElement element, String valueToSet) {
		clearAndSendKeysTo(element, valueToSet);
		try {
			webDriverWait(element).until(ExpectedConditions.attributeToBe(element, "value", valueToSet));
		} catch (TimeoutException e) {
			throw new TimeoutException("Value " + valueToSet + " was not set to element");
		}
	}

	public String getTextFrom(WebElement element) {
		return element.getText();
	}

	public static List<String> getTextFrom(List<WebElement> elements) {
		return elements.stream().map(WebElement::getText).collect(toList());
	}

	public String getTextFromAndTrim(WebElement element) {
		return element.getText().trim();
	}

	public List<String> getTextFromAndTrim(List<WebElement> elements) {
		return elements.stream().map(this::getTextFromAndTrim).collect(toList());
	}

	/**
	 * Returns the value of web element.
	 * 
	 * @param element
	 * @return value
	 */
	public String getValueOf(WebElement element) {
		return element.getAttribute("value");
	}

	public WebElement waitForVisibilityOf(WebElement element) {
		return waitForVisibilityOf(element, DEFAULT_TIMEOUT_TO_WAIT);
	}

	public WebElement waitForVisibilityOf(WebElement element, long timeoutInSeconds) {
		try {
			webDriverWait(element, timeoutInSeconds).until(ExpectedConditions.visibilityOf(element));
		} catch (TimeoutException e) {
			// nothing to do here
		}

		return element;
	}

	/**
	 * TODO: Needs to be tested!!!
	 * 
	 * Waits for element to be invisible.
	 * 
	 * @param element
	 *            waits for this element
	 * @param timeInSeconds
	 *            maximum time in seconds to wait
	 */
	public void waitForInvisibilityOf(WebElement element, long timeInSeconds) {
		try {
			webDriverWait(element, timeInSeconds).ignoring(NoSuchElementException.class)
					.until(ExpectedConditions.not((WebDriver d) -> element.isDisplayed()));
		} catch (TimeoutException e) {
			// nothing to do
		}
	}

	/**
	 * Waits for element to be invisible. Uses <code>DEFAULT_TIMEOUT_TO_WAIT</code>
	 * as maximum time to wait.
	 * 
	 * @param element
	 *            waits for this element
	 */
	public void waitForInvisibilityOf(WebElement element) {
		this.waitForInvisibilityOf(element, DEFAULT_TIMEOUT_TO_WAIT);
	}

	private JavascriptExecutor getJavascriptExecutor(WebElement element) {
		return (JavascriptExecutor) unpackWebDriverFromSearchContext(element);
	}

	/**
	 * Moves a web element into view with the help of JavaScript
	 * 
	 * @param e
	 * @return webelement
	 */
	public WebElement scrollIntoView(WebElement e) {
		getJavascriptExecutor(e).executeScript("arguments[0].scrollIntoView(true)", e);
		return e;
	}

	/**
	 * Moves a web element into view with the help of JavaScript
	 * 
	 * Note: use this method when element shows on page but it is overlapped by
	 * another
	 * 
	 * @param e
	 * @return webelement
	 */
	public WebElement scrollIntoViewReverse(WebElement e) {
		getJavascriptExecutor(e).executeScript("arguments[0].scrollIntoView(false)", e);
		return e;
	}

	/**
	 * Read inner HTML from an element. The method is useful if text needs to be
	 * read from a not visible element.
	 * 
	 * @param element
	 * @return
	 */
	public String getInnerHtmlText(WebElement element) {
		return String.class
				.cast(getJavascriptExecutor(element).executeScript("return arguments[0].innerHTML", element));
	}

	public static String getCssValueFrom(WebElement element, String propertyName) {
		return element.getCssValue(propertyName);
	}

	public static List<String> getCssValueFrom(List<WebElement> elements, String propertyName) {
		return elements.stream().map(e -> WebElementUtils.getCssValueFrom(e, propertyName)).collect(toList());
	}

	/**
	 * Returns css value of {@code propertyName} from {@code pseudoElement}, e.g.
	 * ::after. To read more about Pseudo Elements, refer
	 * http://www.w3schools.com/css/css_pseudo_elements.asp
	 * 
	 * 
	 * @param webElement
	 *            to lookup for pseudo element css values
	 * @param pseudoElement
	 *            e.g. ::after
	 * @param propertyName
	 *            e.g. content
	 * @return css value
	 */
	public static String getCssValueOfPseudoElement(WebElement webElement, String pseudoElement, String propertyName) {
		JavascriptExecutor js = (JavascriptExecutor) WebDriverUnpackUtility
				.unpackWebDriverFromSearchContext(webElement);
		String script = "return window.getComputedStyle(arguments[0],'" + pseudoElement + "').getPropertyValue('"
				+ propertyName + "')";
		String value = (String) js.executeScript(script, webElement);

		return value;
	}

	public static String getPropertyFromJsDataObject(WebElement webElement, String property) {
		JavascriptExecutor js = (JavascriptExecutor) WebDriverUnpackUtility
				.unpackWebDriverFromSearchContext(webElement);
		String script = "return arguments[0].__data__;";
		Object data = js.executeScript(script, webElement);

		if (data instanceof Map) {
			Map<?, ?> values = (Map<?, ?>) data;
			return String.valueOf(values.get(property));
		} else {
			throw new RuntimeException("\n__data__ property is not a map\n");
		}

	}

	public void hover(WebElement webElement) {
		scrollIntoView(webElement);
		new Actions(WebDriverUnpackUtility.unpackWebDriverFromSearchContext(webElement)).moveToElement(webElement)
				.perform();
	}

	/**
	 * Moves mouse first to element {@code toStart} then to element {@code toFinish}
	 * 
	 * @param toStart
	 * @param toFinish
	 */
	public void moveBetween(WebElement toStart, WebElement toFinish) {
		new Actions(WebDriverUnpackUtility.unpackWebDriverFromSearchContext(toStart)).moveToElement(toStart)
				.moveToElement(toFinish).perform();
	}

	public String getAttribute(WebElement element, String attribute) {
		return element.getAttribute(attribute);
	}
}
