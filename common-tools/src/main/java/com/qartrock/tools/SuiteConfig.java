package com.qartrock.tools;

import java.util.List;

import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlSuite;

/**
 * This is a testng listener to modify 'testng.xml' file in runtime.
 * 
 * @author rromanik
 *
 */
public class SuiteConfig implements IAlterSuiteListener {

//	@Override
//	public void alter(List<XmlSuite> suites) {
//		// adapts test suite based on tenant configuration
//		for (XmlSuite suite : suites) {
//			adaptTestSuiteForTenant(suite);
//		}
//	}
	
	@Override
	public void alter(List<XmlSuite> suites) {
		for (XmlSuite suite : suites) {
			BrowserExcludeUtils.exclude(suite);
		}
	}

//	private void adaptTestSuiteForTenant(XmlSuite suite) {
//		List<XmlTest> original = suite.getTests();
//		List<XmlTest> tests = Lists.newArrayList(original);
//		List<String> testsToExclude = TenancyConfigurationManager.getTestsToExclude();
//
//		tests.stream().filter(t -> testsToExclude.contains(t.getName())).forEach(original::remove);
//	}
}