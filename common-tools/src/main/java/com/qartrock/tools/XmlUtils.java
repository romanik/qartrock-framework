package com.qartrock.tools;

import java.util.Base64;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public interface XmlUtils {
	// private XmlUtils() {
	//
	// }

	public static void copyXmlNodeContent(String tagName, Element src, Element dest) {
		copyXmlNodeContent(tagName, src, tagName, dest);
	}

	public static void copyXmlNodeContent(String srcTagName, Element src, String destTagName, Element dest) {
		String content = src.getElementsByTagName(srcTagName).item(0).getTextContent();
		dest.getElementsByTagName(destTagName).item(0).setTextContent(content);
	}

	public static void copyFromNodeToAttribute(String srcTagName, Element src, String destAttribute, Element dest) {
		String content = src.getElementsByTagName(srcTagName).item(0).getTextContent();
		dest.setAttribute(destAttribute, content);
	}

	public static void addTextElement(Element element, String tagName, String value) {
		Element child = element.getOwnerDocument().createElement(tagName);
		child.setTextContent(value);
		element.appendChild(child);
	}

	public static String xmlToBase64(Node element) {
		return Base64.getEncoder().encodeToString(XmlParser.getStringFromDocument(element).getBytes());
	}
}
