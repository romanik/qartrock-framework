package com.qartrock.tools.reports.helpers;

import java.util.Collection;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.common.collect.Multimap;

import ru.yandex.qatools.allure.model.Status;
import ru.yandex.qatools.allure.model.TestCaseResult;

/**
 * This class creates a workbook and writes test results statistics into it. The
 * created workbook contains the following tabs:
 * <ul>
 * <li>Failed Tests Statistics</li>
 * <li>All Tests Statistics</li>
 * </ul>
 * 
 * @author rromanik
 *
 */
public class TestResultsToExcelWriter {

	private static final int TEST_ID_COLUMN_INDEX = 0;

	private static final int STATUS_COLUMN_INDEX = 1;

	private static final int TEST_METHOD_NAME_COLUMN_INDEX = 2;

	/**
	 * Issue ids may extend to several cells, if one method has a few issues
	 */
	private static final int ISSUE_ID_INITIAL_COLUMN_INDEX = 3;

	private int allTestsRowCounter = 1;

	private int failedTestsRowCounter = 1;

	private Workbook wb;

	private Sheet failedTestsSheet;

	private Sheet allTestsSheet;

	private boolean idInserted;

	private boolean failIdInserted;

	private final Multimap<String, TestCaseResult> idResultMap;

	private TestResultsToExcelWriter(Multimap<String, TestCaseResult> idResultMap) {
		this.idResultMap = idResultMap;
	}

	public static Workbook createWorkbook(Multimap<String, TestCaseResult> idResultMap) {
		return new TestResultsToExcelWriter(idResultMap).createWorkbook();
	}

	private Workbook createWorkbook() {
		wb = new XSSFWorkbook();
		failedTestsSheet = wb.createSheet("Failed_Tests_Statistics");
		allTestsSheet = wb.createSheet("All_Tests_Statistics");

		for (String testCaseId : idResultMap.keySet()) {
			writeTestCaseResultsWithSameId(testCaseId);
		}
		return wb;
	}

	private void writeTestCaseResultsWithSameId(String testCaseId) {
		idInserted = false;
		failIdInserted = false;

		Collection<TestCaseResult> resultsWithSameId = idResultMap.get(testCaseId);
		for (TestCaseResult res : resultsWithSameId) {
			writeToAllResults(testCaseId, res);
			writeToFailedResults(testCaseId, res);
		}
	}

	private void writeToAllResults(String testCaseId, TestCaseResult res) {
		Row row = allTestsSheet.createRow(allTestsRowCounter++);
		if (!idInserted) {
			writeTestIdLink(testCaseId, row);
			idInserted = true;
		}
		writeIntoRow(res, row);
	}

	private void writeToFailedResults(String testCaseId, TestCaseResult res) {
		if (res.getStatus().equals(Status.PASSED))
			return;

		Row failRow = failedTestsSheet.createRow(failedTestsRowCounter++);
		if (!failIdInserted) {
			writeTestIdLink(testCaseId, failRow);
			failIdInserted = true;
		}
		writeIntoRow(res, failRow);
	}

	private void writeTestIdLink(String testCaseId, Row row) {
		writeLink(HardCodedPatterns.TMS_PATTERN, testCaseId, row, TEST_ID_COLUMN_INDEX);
	}

	private void writeIssueIdLink(String issueId, Row row, int cellIndex) {
		writeLink(HardCodedPatterns.ISSUE_TRACKER_PATTERN, issueId, row, cellIndex);
	}

	private void writeLink(String pattern, String id, Row row, int cellIndex) {
		Hyperlink link = getHyperlink(pattern, id);
		CellStyle hlinkStyle = getHyperlinkStyle();

		Cell linkCell = row.createCell(cellIndex);
		linkCell.setCellValue(id);
		linkCell.setHyperlink(link);
		linkCell.setCellStyle(hlinkStyle);
	}

	private Hyperlink getHyperlink(String format, String id) {
		CreationHelper creationHelper = wb.getCreationHelper();
		Hyperlink link = creationHelper.createHyperlink(HyperlinkType.URL);
		link.setAddress(String.format(format, id));
		return link;
	}

	private CellStyle getHyperlinkStyle() {
		CellStyle hlinkStyle = wb.createCellStyle();
		Font hlinkFont = wb.createFont();
		hlinkFont.setUnderline(Font.U_SINGLE);
		hlinkFont.setColor(IndexedColors.BLUE.getIndex());
		hlinkStyle.setFont(hlinkFont);
		return hlinkStyle;
	}

	private void writeIntoRow(TestCaseResult res, Row row) {
		row.createCell(STATUS_COLUMN_INDEX).setCellValue(res.getStatus().toString());
		row.createCell(TEST_METHOD_NAME_COLUMN_INDEX).setCellValue(res.getName());

		int j = ISSUE_ID_INITIAL_COLUMN_INDEX;
		for (String issue : TestCaseResultUtils.getIssues(res)) {
			writeIssueIdLink(issue, row, j++);
		}
	}

}
