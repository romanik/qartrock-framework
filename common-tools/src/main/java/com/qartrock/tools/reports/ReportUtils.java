package com.qartrock.tools.reports;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.List;

import org.testng.ITestResult;
import org.uncommons.reportng.ReportNGUtils;

import ru.yandex.qatools.allure.annotations.TestCaseId;

public class ReportUtils extends ReportNGUtils {

	/**
	 * Test Case Id as definded by {@link TestCaseId} annotation over a test
	 * method
	 */
	public static final String TEST_CASE_ID_ATTRIBUTE = "testcaseid";

	@Override
	public List<String> getTestOutput(ITestResult result) {
		List<String> output = super.getTestOutput(result);

		// Test Case ID
		String testCaseId = (String) result.getAttribute(TEST_CASE_ID_ATTRIBUTE);
		if (isNotEmpty(testCaseId)) {
			output.add("<b>ID</b>: " + testCaseId + "<br>");
		}

		return output;
	}
}