package com.qartrock.tools.reports.helpers;

import static com.qartrock.tools.reports.helpers.HardCodedPatterns.ISSUE_TRACKER_PATTERN;
import static com.qartrock.tools.reports.helpers.HardCodedPatterns.TMS_PATTERN;
import static j2html.TagCreator.a;
import static j2html.TagCreator.td;
import static j2html.TagCreator.th;
import static j2html.TagCreator.tr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import j2html.tags.ContainerTag;
import j2html.tags.DomContentJoiner;
import ru.yandex.qatools.allure.model.Status;
import ru.yandex.qatools.allure.model.TestCaseResult;

public class IssueStatusStatById {
	private final String id;

	private final List<String> issues = new ArrayList<>();
	private final List<Status> statuses = new ArrayList<>();
	private final List<String> names = new ArrayList<>();

	private final List<Pair<Status, List<String>>> statusIssuesPairs = new ArrayList<>();
	private final Collection<TestCaseResult> tcs;

	public IssueStatusStatById(String id, Collection<TestCaseResult> tcs) {
		this.id = id;
		this.tcs = tcs;
		parse();
	}

	private void parse() {
		List<String> oneTCIssues;
		for (TestCaseResult tc : tcs) {
			oneTCIssues = TestCaseResultUtils.getIssues(tc);
			statusIssuesPairs.add(new ImmutablePair<Status, List<String>>(tc.getStatus(), oneTCIssues));
			issues.addAll(oneTCIssues);
			statuses.add(tc.getStatus());
			names.add(tc.getName());
		}
	}

	public String getId() {
		return this.id;
	}

	public ContainerTag getTmsLink() {
		String href = String.format(TMS_PATTERN, getId());
		return a(getId()).withHref(href);
	}

	public List<String> getAllIssues() {
		return issues;
	}

	private ContainerTag getIssueLinks(List<String> issues) {
		if (issues.isEmpty())
			return td();
		List<ContainerTag> links = new ArrayList<>();

		for (String issue : issues) {
			links.add(a(issue).withHref(String.format(ISSUE_TRACKER_PATTERN, issue)));
		}

		return td(links.toArray(new ContainerTag[0]));
	}

	public List<Status> getStatuses() {
		return statuses;
	}

	public boolean isSuccessful() {
		return getStatuses().stream().allMatch(s -> s.equals(Status.PASSED));
	}

	public List<ContainerTag> asHtmlRows() {
		List<ContainerTag> rows = new ArrayList<>();

		int tcNumber = statusIssuesPairs.size();
		for (int i = 0; i < tcNumber; i++) {
			Pair<Status, List<String>> p = statusIssuesPairs.get(i);
			ContainerTag tmsLinkOrEmptyTd = (i == 0) ? td(getTmsLink()) : td();
			ContainerTag tdWithIssues = getIssueLinks(p.getValue());
			ContainerTag tr = tr(tmsLinkOrEmptyTd, td(names.get(i)), td(p.getKey().toString()), tdWithIssues);
			rows.add(tr);
		}
		return rows;
	}

	/**
	 * Returns a list of this statistics objects as HTML rows with statuses
	 * other than PASSED
	 * 
	 * @return list of not passed test case results
	 */
	public List<ContainerTag> asHtmlRowsExcludingSuccess() {
		List<ContainerTag> rows = new ArrayList<>();
		if (isSuccessful())
			return rows;

		int tcNumber = statusIssuesPairs.size();
		for (int i = 0; i < tcNumber; i++) {
			Pair<Status, List<String>> p = statusIssuesPairs.get(i);
			if (p.getKey().equals(Status.PASSED))
				continue;
			ContainerTag tmsLinkOrEmptyTd = rows.isEmpty() ? td(getTmsLink()) : td();
			ContainerTag tdWithIssues = getIssueLinks(p.getValue());
			ContainerTag tr = tr(tmsLinkOrEmptyTd, td(names.get(i)), td(p.getKey().toString()), tdWithIssues);
			rows.add(tr);
		}
		return rows;
	}

	public static ContainerTag getTableHeaderRow() {
		return tr(th("Id"), th("Name"), th("Statuses"), th("Issues"));
	}

	@Override
	public String toString() {
		return DomContentJoiner.join("\n", true, asHtmlRows().toArray(new Object[0])).render();
	}

	public static void main(String[] args) {
		System.out.println(getTableHeaderRow().renderFormatted());
	}
}