package com.qartrock.tools.reports;

import static j2html.TagCreator.b;
import static j2html.TagCreator.caption;
import static j2html.TagCreator.div;
import static j2html.TagCreator.p;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Workbook;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.qartrock.tools.reports.helpers.IssueStatusStatById;
import com.qartrock.tools.reports.helpers.TestCaseResultUtils;
import com.qartrock.tools.reports.helpers.TestResultsToExcelWriter;

import j2html.TagCreator;
import j2html.tags.ContainerTag;
import ru.yandex.qatools.allure.commons.AllureFileUtils;
import ru.yandex.qatools.allure.model.TestCaseResult;
import ru.yandex.qatools.allure.model.TestSuiteResult;
import ru.yandex.qatools.allure.utils.AllureResultsUtils;

public class TestRunStatistics {

	private final Multimap<String, TestCaseResult> testId_testCase_Map;

	private final Multimap<String, TestCaseResult> noTestIdTestCases_Map;

	private final List<TestSuiteResult> suites;

	private List<IssueStatusStatById> issueStatusStats;

	private TestRunStatistics(List<TestSuiteResult> suites) {
		this.suites = suites;
		testId_testCase_Map = MultimapBuilder.hashKeys().arrayListValues().build();
		noTestIdTestCases_Map = MultimapBuilder.hashKeys().arrayListValues().build();
		createStatistics();
	}

	public static void main(String[] args) throws IOException {
		// TestRunStatistics s = createFromResults(new File(
		// "C:\\Users\\QArtrock03\\workspace\\eclipse_neon\\dataclarity_dashinsight\\tests\\target\\allure-results"));
		TestRunStatistics s = createFromResults(
				new File("C:\\Users\\QArtrock03\\DataClarity\\jenkins_stats\\12jan2018\\allure-results"));
		// s.getStatisticsById().stream().forEach(System.out::println);
		// System.out.println(s.generalInformation());
		// System.out.println(s.asFailuresHtmlTable());
		// s.allTestIds().forEach(System.out::println);
		s.writeToDirectory(new File("C:\\Users\\QArtrock03\\DataClarity\\jenkins_stats\\12jan2018"));
	}

	public static TestRunStatistics create() {
		File resultsDir = AllureResultsUtils.getResultsDirectory();

		return createFromResults(resultsDir);
	}

	public static TestRunStatistics createFromResults(File resultsDirectory) {
		List<TestSuiteResult> suites = getSuites(resultsDirectory);
		return new TestRunStatistics(suites);
	}

	private static List<TestSuiteResult> getSuites(File resultsDirectory) {
		try {
			return AllureFileUtils.unmarshalSuites(resultsDirectory);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return Collections.emptyList();
		}
	}

	public List<IssueStatusStatById> getStatisticsById() {
		if (issueStatusStats != null)
			return issueStatusStats;
		issueStatusStats = new ArrayList<>();
		for (String id : testId_testCase_Map.keySet()) {
			issueStatusStats.add(new IssueStatusStatById(id, testId_testCase_Map.get(id)));
		}
		return issueStatusStats;
	}

	public List<IssueStatusStatById> getNotSuccessfulStatsById() {
		return getStatisticsById().stream().filter(el -> !el.isSuccessful()).collect(Collectors.toList());
	}

	public List<String> allTestIds() {
		return getStatisticsById().stream().map(el -> el.getId()).collect(Collectors.toList());
	}

	public int allTestNumber() {
		return getStatisticsById().size();
	}

	public int notSuccessfulTestNumber() {
		return getNotSuccessfulStatsById().size();
	}

	public String generalInformation() {
		return div(p(b("Tests executed: ")).withText(String.valueOf(allTestNumber())),
				p(b("Tests failed:   ")).withText(String.valueOf(notSuccessfulTestNumber()))).renderFormatted();
	}

	public String asHtmlTable() {
		List<IssueStatusStatById> rows = getStatisticsById();

		return asHtmlTable(rows, "Test Run Statistics");
	}

	public String asFailuresHtmlTable() {
		List<IssueStatusStatById> rows = getNotSuccessfulStatsById();

		return asHtmlTableExcludingSuccess(rows, "Test Failure Statistics");
	}

	private String asHtmlTableExcludingSuccess(List<IssueStatusStatById> rows, String caption) {
		List<ContainerTag> tags = new ArrayList<>();
		tags.add(caption(b(caption)));
		tags.add(IssueStatusStatById.getTableHeaderRow());

		for (IssueStatusStatById row : rows) {
			for (ContainerTag r : row.asHtmlRowsExcludingSuccess()) {
				tags.add(r);
			}
		}
		return TagCreator.table(tags.toArray(new ContainerTag[0])).attr("border", 1).renderFormatted();
	}

	private String asHtmlTable(List<IssueStatusStatById> rows, String caption) {
		List<ContainerTag> tags = new ArrayList<>();
		tags.add(caption(b(caption)));
		tags.add(IssueStatusStatById.getTableHeaderRow());

		for (IssueStatusStatById row : rows) {
			for (ContainerTag r : row.asHtmlRows()) {
				tags.add(r);
			}
		}
		return TagCreator.table(tags.toArray(new ContainerTag[0])).attr("border", 1).renderFormatted();
	}

	private void createStatistics() {
		for (TestSuiteResult suiteResult : suites) {
			getStatsForSuite(suiteResult);
		}
	}

	private void getStatsForSuite(TestSuiteResult suiteResult) {
		List<TestCaseResult> tcs = suiteResult.getTestCases();
		for (TestCaseResult tc : tcs) {
			getStatsForTestCase(tc);
		}
	}

	private void getStatsForTestCase(TestCaseResult tc) {
		List<String> testId = TestCaseResultUtils.getTestId(tc);

		if (testId.size() == 0)
			noTestIdTestCases_Map.put(tc.getName(), tc);

		if (testId.size() == 1)
			testId_testCase_Map.put(testId.get(0), tc);
	}

	public void writeToAllureResultsDirectory() {
		String path = System.getProperty("user.dir") + File.separator + ".." + File.separator + "allure-report";
		File dir = new File(path);
		if (!dir.exists())
			dir.mkdir();
		writeToDirectory(dir);

		Workbook wb = TestResultsToExcelWriter.createWorkbook(this.testId_testCase_Map);

		try (FileOutputStream fileOut = new FileOutputStream(
				path + File.separator + "TestRunCustomizedStatistics.xlsx");) {
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void writeToDirectory(File dir) {
		String statisticsFilePath = dir.getAbsolutePath() + File.separator + "test-run-customized-statistics.html";
		try (FileWriter fw = new FileWriter(statisticsFilePath, true)) {
			fw.write(generalInformation());
			fw.write(this.asFailuresHtmlTable());
			fw.write(this.asHtmlTable());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// public static void main(String[] args) {
	// System.out.println(System.getProperty("user.dir"));
	// }

}