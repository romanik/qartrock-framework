package com.qartrock.tools.reports.helpers;

public class HardCodedPatterns {

	public static final String TMS_PATTERN = "https://dataclarity.testrail.com/index.php?/cases/view/%s";
	
	public static final String ISSUE_TRACKER_PATTERN = "https://dataclarity.jira.com/browse/%s";
}