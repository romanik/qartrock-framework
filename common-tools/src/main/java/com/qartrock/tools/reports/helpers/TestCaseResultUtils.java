package com.qartrock.tools.reports.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ru.yandex.qatools.allure.model.Label;
import ru.yandex.qatools.allure.model.LabelName;
import ru.yandex.qatools.allure.model.TestCaseResult;

public class TestCaseResultUtils {
	public static List<String> getTestId(TestCaseResult tc) {
		return tc.getLabels().stream().filter(l -> l.getName().equals(LabelName.TEST_ID.value())).map(Label::getValue)
				.collect(Collectors.toList());
	}

	public static List<String> getIssues(TestCaseResult tc) {
		return tc.getLabels().stream().filter(l -> l.getName().equals(LabelName.ISSUE.value())).map(Label::getValue)
				.collect(Collectors.toCollection(ArrayList::new));
	}
}