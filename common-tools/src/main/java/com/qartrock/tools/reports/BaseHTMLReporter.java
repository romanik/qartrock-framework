package com.qartrock.tools.reports;

import static com.qartrock.tools.reports.ReportUtils.TEST_CASE_ID_ATTRIBUTE;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.uncommons.reportng.HTMLReporter;

import com.qartrock.tools.ConfigurationManager;
import com.qartrock.tools.pageobject.AbstractApp;

import ru.yandex.qatools.allure.annotations.TestCaseId;

public class BaseHTMLReporter extends HTMLReporter implements ITestListener {
	private static final String UTILS_KEY = "utils";
	private static final ReportUtils REPORT_UTILS = new ReportUtils();

	@Override
	protected VelocityContext createContext() {
		VelocityContext context = super.createContext();
		context.put(UTILS_KEY, REPORT_UTILS);
		return context;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTestStart(ITestResult result) {
		try {
			System.out.println("Start: " + result.getName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTestSuccess(ITestResult result) {
		testId(result);
		if (ConfigurationManager.takeScreenshotOnSuccess()) {
			createScreenshot(result, "Success Screenshot");
		}
		addJsLogs(result);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTestFailure(ITestResult result) {
		testId(result);
		createScreenshot(result, "Failure Screenshot");
		addJsLogs(result);
		try {
			System.out.println("Failed: " + result.getInstanceName() + " : " + result.getName());
		} catch (Exception ignored) {
			System.out.println("In BaseHTMLReporter#onTestSuccess");
		}
		logToConsole("Failed", result);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTestSkipped(ITestResult result) {
		testId(result);
		createScreenshot(result, "Skip Screenshot");
		addJsLogs(result);
		logToConsole("Skipped", result);
	}
	
	private void logToConsole(String status, ITestResult result) {
		try {
			System.out.println(status + ": " + result.getInstanceName() + " : " + result.getName());
		} catch (Exception ignored) {
			System.out.println("In BaseHTMLReporter#onTest" + status);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onStart(ITestContext context) {
		try {
			System.out.println("Start test: " + context.getName());
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#onStart:\n" + e.getMessage());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFinish(ITestContext context) {
		try {
			System.out.println("Finish: " + context.getName());
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#onFinish:\n" + e.getMessage());
		}
	}

	/**
	 * Adds the Test Case Id attribute for TestResult to eventually appear in
	 * ReportNG report.
	 * 
	 * @param result
	 */
	private void testId(ITestResult result) {
		try {
			TestCaseId id = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestCaseId.class);

			if (id != null)
				result.setAttribute(TEST_CASE_ID_ATTRIBUTE, id.value());
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#testId:\n" + e.getMessage());
		}
	}

	/**
	 * Adds the screenshot safely to the Allure report
	 * 
	 * @param result
	 * @param name
	 */
	private void createScreenshot(ITestResult result, String name) {
		if (!AbstractApp.canTakeScreenshot())
			return;
		try {
			AllureUtils.makeScreenshot(name, AbstractApp.getDriver());
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#createScreenshot:\n" + e.getMessage());
		}
	}

	/**
	 * Adds browser logs to the Allure report. So far, it works only with
	 * {@link ChromeDriver} and {@link FirefoxDriver}
	 * 
	 * @param result
	 */
	private void addJsLogs(ITestResult result) {
		if (!AbstractApp.canTakeScreenshot())
			return;
		try {
			WebDriver driver = AbstractApp.getDriver();

			if (driver instanceof ChromeDriver) {
				LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
				if (!logEntries.getAll().isEmpty()) {
					AllureUtils.attachBrowserLogs(logEntries, "Chrome");
				}
			}

			else if (driver instanceof FirefoxDriver) {
				String logs = AbstractApp.getConsoleLogs();
				if (StringUtils.containsIgnoreCase(logs, "JavaScript error")) {
					AllureUtils.attach("Firefox Browser logs", logs);
				}
			}

			// else if (driver instanceof InternetExplorerDriver) {
			// String logs = AbstractApp.getConsoleLogs();
			// if (StringUtils.containsIgnoreCase(logs, "JavaScript error")) {
			// AllureUtils.attach("IE logs", logs);
			// }
			// }

			// else if (driver instanceof RemoteWebDriver) {
			// String logs = AbstractApp.getConsoleLogs();
			// if (StringUtils.containsIgnoreCase(logs, "JavaScript error")) {
			// AllureUtils.attach("Remote Driver logs", logs);
			// }
			// }
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#addJsLogs:\n" + e.getMessage());
		}
	}
}
