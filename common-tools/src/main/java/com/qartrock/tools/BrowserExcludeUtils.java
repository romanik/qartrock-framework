package com.qartrock.tools;

import java.lang.annotation.Annotation;
import java.util.List;

import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.google.common.collect.Lists;
import com.qartrock.tools.browser.DriverType;

public class BrowserExcludeUtils {
	
	private static DriverType runBrowser;
	
	public static void exclude(XmlSuite suite) {
		runBrowser = ConfigurationManager.getDriverType();
		List<XmlTest> xmlTests = suite.getTests();
		
		for(XmlTest xmlTest : xmlTests) {
			List<XmlClass> xmlClasses = xmlTest.getClasses();
			List<XmlClass> copy = Lists.newArrayList(xmlClasses);
			for(XmlClass xmlClass : copy) {
				if (shouldBeExcluded(xmlClass)) {
					xmlClasses.remove(xmlClass);
				}
			}
		}
	}
	
	private static boolean shouldBeExcluded(XmlClass xmlClass) {
		Class<?> testClass = getTestClass(xmlClass);
		if (testClass.isAnnotationPresent(ExcludeBrowsers.class)) {
			Annotation annotation = testClass.getAnnotation(ExcludeBrowsers.class);
			ExcludeBrowsers excludeBrowsers = (ExcludeBrowsers) annotation;
			DriverType[] browsers = excludeBrowsers.browser();
			for (int i=0; i<browsers.length; i++) {
				if (browsers[i].equals(runBrowser)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static Class<?> getTestClass(XmlClass xmlClass) {
		String testClassName = xmlClass.getName();
		Class<?> testClass = null;
		try {
			testClass = Class.forName(testClassName);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		return testClass;
	}
	
}
