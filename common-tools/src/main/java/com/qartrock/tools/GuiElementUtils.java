package com.qartrock.tools;

import static io.appium.java_client.pagefactory.utils.WebDriverUnpackUtility.unpackWebDriverFromSearchContext;

import org.openqa.selenium.interactions.Actions;

import com.qartrock.tools.pageobject.AbstractGuiElement;
import com.qartrock.tools.pageobject.GuiElement;

public class GuiElementUtils {

	/**
	 * Drags and drops {@link AbstractGuiElement} element to
	 * {@link AbstractGuiElement} target.
	 * 
	 * @param element
	 * @param target
	 */
	public void dragAndDrop(AbstractGuiElement element, AbstractGuiElement target) {
		getActions(element).dragAndDrop(element.getWrappedElement(), target.getWrappedElement()).build().perform();
	}

	/**
	 * Drags and drops {@link GuiElement} element to {@link GuiElement} target.
	 * 
	 * @param element
	 * @param target
	 */
	public void dragAndDrop(GuiElement element, GuiElement target) {
		if (!(element instanceof AbstractGuiElement)) {
			throw new IllegalArgumentException(
					"Element to drag should be instance of " + AbstractGuiElement.class.getName());
		}

		if (!(target instanceof AbstractGuiElement)) {
			throw new IllegalArgumentException(
					"Targat element to perform drag and drop should be instance of " + AbstractGuiElement.class.getName());
		}

		dragAndDrop(AbstractGuiElement.class.cast(element), AbstractGuiElement.class.cast(target));
	}

	/**
	 * Creates {@link Actions} object based on given {@link AbstractGuiElement}
	 * {@code element}.
	 * 
	 * @param element
	 * @return actions object reference
	 */
	private Actions getActions(AbstractGuiElement element) {
		Actions actions = new Actions(unpackWebDriverFromSearchContext(element));
		return actions;
	}
}
