package com.qartrock.exeptions;

import org.openqa.selenium.TimeoutException;

public class PageNotLoadedException extends TimeoutException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PageNotLoadedException() {
		super();
	}

	public PageNotLoadedException(String message, Throwable cause) {
		super(message, cause);
	}

	public PageNotLoadedException(String message) {
		super(message);
	}

	public PageNotLoadedException(Throwable cause) {
		super(cause);
	}

}
