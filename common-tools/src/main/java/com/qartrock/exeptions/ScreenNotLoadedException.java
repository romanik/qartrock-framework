package com.qartrock.exeptions;

import org.openqa.selenium.TimeoutException;

public class ScreenNotLoadedException extends TimeoutException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScreenNotLoadedException() {
		super();
	}

	public ScreenNotLoadedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ScreenNotLoadedException(String message) {
		super(message);
	}

	public ScreenNotLoadedException(Throwable cause) {
		super(cause);
	}

}
