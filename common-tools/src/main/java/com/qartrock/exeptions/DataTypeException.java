package com.qartrock.exeptions;

public class DataTypeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataTypeException() {
		super();
	}

	public DataTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DataTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataTypeException(String message) {
		super(message);
	}

	public DataTypeException(Throwable cause) {
		super(cause);
	}

}
