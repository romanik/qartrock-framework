package com.qartrock.data;

public class UserModel {

	private final String username;
	private final String password;
	private final String name;
	private final String role;

	/**
	 * 
	 * @param username
	 * @param password
	 * @param name
	 */
	public UserModel(String username, String password, String name) {
		this(username, password, name, true);
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @param name
	 * @param inUse
	 */
	public UserModel(String username, String password, String name, Boolean inUse) {
		this(username, password, name, "not specified", inUse);
	}

	public UserModel(String username, String password, String name, String role, Boolean available) {
		this.username = username;
		this.password = password;
		this.name = name;
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public String getRole() {
		return this.role;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password.replaceAll("\\w", "*") + ", name=" + name
				+ ", role=" + role + "]";
	}

}
